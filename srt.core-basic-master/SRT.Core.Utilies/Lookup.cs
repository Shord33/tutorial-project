﻿using System;
using System.Collections.Generic;

namespace SRT.Core.Utilies
{
    public class Lookup
    {
        public IEnumerable<KeyValuePair<string, string>> BindLookupToKeyPair(Dictionary<string, string> dic)
        {
            List<KeyValuePair<string, string>> keypairValues = new List<KeyValuePair<string, string>>();

            if (dic != null)
            {
                foreach (var t in dic)
                {
                    keypairValues.Add(new KeyValuePair<string, string>(t.Key, t.Value));
                }
            }

            return keypairValues;
        }
    }
}
