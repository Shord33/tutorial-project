﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Utilies.ExtensionClass
{
    public class EmailStructure
    {
        public EmailStructure()
        {
            RecipientTo = new List<string>();
        }

        public string EmailFrom { get; set; }
        public string EmailFromName { get; set; }
        public List<string> RecipientTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
