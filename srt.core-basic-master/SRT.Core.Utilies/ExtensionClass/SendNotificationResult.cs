﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Utilies.ExtensionClass
{
    public class SendNotificationResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
