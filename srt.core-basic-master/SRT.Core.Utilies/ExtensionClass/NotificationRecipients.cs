﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Utilies.ExtensionClass
{
    public class NotificationRecipients
    {
        public Guid IdRecipient { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public SendNotificationResult Result { get; set; }
    }
}
