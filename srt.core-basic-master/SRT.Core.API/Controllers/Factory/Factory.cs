﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Factory
{
    [Route("api/[controller]")]
    [ApiController]
    public class Factory
    {
        private readonly ILogger<Services.Factory.Factory> _log;
        private Services.Factory.Contract.IFactory _factory;

        public Factory(ILogger<Services.Factory.Factory> log, Services.Factory.Contract.IFactory factory)
        {
            this._log = log;
            this._factory = factory;
        }

        [HttpGet]
        public IEnumerable<Services.Factory.DTO.Factory> Get()
        {
            try
            {
                return this._factory.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Factory.DTO.Factory Get(Guid Id)
        {
            try
            {
                return this._factory.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Factory.DTO.Factory> GetActiveRefObjectState()
        {
            try
            {
                return this._factory.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Factory.DTO.Factory> GetTerminatedRefObjectState()
        {
            try
            {
                return this._factory.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Factory.DTO.Factory> GetbyName(string name)
        {
            try
            {
                return this._factory.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Factory.DTO.Factory> ContainsName(string keyword)
        {
            try
            {
                return this._factory.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Factory.DTO.Factory> ContainsAliasName(string keyword)
        {
            try
            {
                return this._factory.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Factory.DTO.Factory> ContainsDescription(string keyword)
        {
            try
            {
                return this._factory.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Factory.DTO.Factory entityType, Guid updater)
        {
            try
            {
                this._factory.Create(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Factory.DTO.Factory> entityType, Guid updater)
        {
            try
            {
                this._factory.CreateInBulk(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Factory.DTO.Factory entityType, Guid updater)
        {
            try
            {
                this._factory.Update(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._factory.Delete(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public void Terminate([FromBody] Services.Factory.DTO.Factory entityType, Guid id, Guid updater)
        {
            try
            {
                this._factory.Terminate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Reactivate/{id}/{updater}")]
        public void Reactivate([FromBody] Services.Factory.DTO.Factory entityType, Guid id, Guid updater)
        {
            try
            {
                this._factory.Reactivate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }
    }
}
