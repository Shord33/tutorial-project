﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.FactoryAttribute
{
    [Route("api/[controller]")]
    [ApiController]
    public class FactoryAttribute
    {
        private readonly ILogger<Services.Factory.FactoryAttribute> _log;
        private Services.Factory.Contract.IFactoryAttribute _factoryAttribute;

        public FactoryAttribute(ILogger<Services.Factory.FactoryAttribute> log, Services.Factory.Contract.IFactoryAttribute factoryAttribute)
        {
            this._log = log;
            this._factoryAttribute = factoryAttribute;
        }

        [HttpGet]
        public IEnumerable<Services.Factory.DTO.FactoryAttribute> Get()
        {
            try
            {
                return this._factoryAttribute.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Factory.DTO.FactoryAttribute Get(Guid Id)
        {
            try
            {
                return this._factoryAttribute.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Factory.DTO.FactoryAttribute> GetActiveRefObjectState()
        {
            try
            {
                return this._factoryAttribute.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Factory.DTO.FactoryAttribute> GetTerminatedRefObjectState()
        {
            try
            {
                return this._factoryAttribute.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Factory.DTO.FactoryAttribute entityType, Guid updater)
        {
            try
            {
                this._factoryAttribute.Create(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Factory.DTO.FactoryAttribute> entityType, Guid updater)
        {
            try
            {
                this._factoryAttribute.CreateInBulk(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Factory.DTO.FactoryAttribute entityType, Guid updater)
        {
            try
            {
                this._factoryAttribute.Update(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._factoryAttribute.Delete(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public void Terminate([FromBody] Services.Factory.DTO.FactoryAttribute entityType, Guid id, Guid updater)
        {
            try
            {
                this._factoryAttribute.Terminate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Reactivate/{id}/{updater}")]
        public void Reactivate([FromBody] Services.Factory.DTO.FactoryAttribute entityType, Guid id, Guid updater)
        {
            try
            {
                this._factoryAttribute.Reactivate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }
    }
}
