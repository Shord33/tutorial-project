﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Area
{
    [Route("api/[controller]")]
    [ApiController]
    public class Area
    {
        private readonly ILogger<Services.Area.Area> _log;
        private Services.Area.Contract.IArea _area;

        public Area(ILogger<Services.Area.Area> log, Services.Area.Contract.IArea area)
        {
            this._log = log;
            this._area = area;
        }

        [HttpGet]
        public IEnumerable<Services.Area.DTO.Area> Get()
        {
            try
            {
                return this._area.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Area.DTO.Area Get(Guid Id)
        {
            try
            {
                return this._area.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Area.DTO.Area> GetActiveRefObjectState()
        {
            try
            {
                return this._area.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Area.DTO.Area> GetTerminatedRefObjectState()
        {
            try
            {
                return this._area.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Area.DTO.Area> GetbyName(string name)
        {
            try
            {
                return this._area.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Area.DTO.Area> ContainsName(string keyword)
        {
            try
            {
                return this._area.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Area.DTO.Area> ContainsAliasName(string keyword)
        {
            try
            {
                return this._area.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Area.DTO.Area> ContainsDescription(string keyword)
        {
            try
            {
                return this._area.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Area.DTO.Area entity, Guid updater)
        {
            try
            {
                this._area.Create(entity, updater);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Area.DTO.Area> entity, Guid updater)
        {
            try
            {
                this._area.CreateInBulk(entity, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Area.DTO.Area entity, Guid updater)
        {
            try
            {
                this._area.Update(entity, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._area.Delete(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public void Terminate([FromBody] Services.Area.DTO.Area entity, Guid id, Guid updater)
        {
            try
            {
                this._area.Terminate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Reactivate/{id}/{updater}")]
        public void Reactivate([FromBody] Services.Area.DTO.Area entity, Guid id, Guid updater)
        {
            try
            {
                this._area.Reactivate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("SetSubArea/{area}/{subArea}")]
        public void SetSubArea(Guid area,Guid subArea,Guid updater)
        {
            try
            {
                this._area.SetSubArea(area, subArea, updater, true);
            }
            catch
            {

            }
        }
    }
}
