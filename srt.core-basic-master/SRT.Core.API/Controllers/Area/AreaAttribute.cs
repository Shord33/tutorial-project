﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.AreaAttribute
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreaAttribute
    {
        private readonly ILogger<Services.Area.AreaAttribute> _log;
        private Services.Area.Contract.IAreaAttribute _areaAttribute;

        public AreaAttribute(ILogger<Services.Area.AreaAttribute> log, Services.Area.Contract.IAreaAttribute areaAttribute)
        {
            this._log = log;
            this._areaAttribute = areaAttribute;
        }

        [HttpGet]
        public IEnumerable<Services.Area.DTO.AreaAttribute> Get()
        {
            try
            {
                return this._areaAttribute.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Area.DTO.AreaAttribute Get(Guid Id)
        {
            try
            {
                return this._areaAttribute.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Area.DTO.AreaAttribute> GetActiveRefObjectState()
        {
            try
            {
                return this._areaAttribute.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Area.DTO.AreaAttribute> GetTerminatedRefObjectState()
        {
            try
            {
                return this._areaAttribute.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Area.DTO.AreaAttribute entityType, Guid updater)
        {
            try
            {
                this._areaAttribute.Create(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Area.DTO.AreaAttribute> entityType, Guid updater)
        {
            try
            {
                this._areaAttribute.CreateInBulk(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Area.DTO.AreaAttribute entityType, Guid updater)
        {
            try
            {
                this._areaAttribute.Update(entityType, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._areaAttribute.Delete(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public void Terminate([FromBody] Services.Area.DTO.AreaAttribute entityType, Guid id, Guid updater)
        {
            try
            {
                this._areaAttribute.Terminate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Reactivate/{id}/{updater}")]
        public void Reactivate([FromBody] Services.Area.DTO.AreaAttribute entityType, Guid id, Guid updater)
        {
            try
            {
                this._areaAttribute.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }
    }
}
