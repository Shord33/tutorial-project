﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SRT.Core.Services.Area.Response;
using SRT.Core.Services.Area.Request;


namespace SRT.Core.API.Controllers.Area
{
    [Route("api/[controller]")]
    [ApiController]
    public class Orchestration : ControllerBase
    {
        private readonly ILogger<Services.Area.Orchestration> _log;
        private Services.Area.Contract.IOrchestration _orchestration;

        public Orchestration(ILogger<Services.Area.Orchestration> log, Services.Area.Contract.IOrchestration orchestration) {
            this._log = log;
            this._orchestration = orchestration;
        }

        [HttpPost("CreateArea")]
        public CreateAreaResponse CreateArea([FromBody] CreateAreaRequest request) {
            CreateAreaResponse response = new CreateAreaResponse();

            try {
                response = this._orchestration.CreateArea(request);
                response.Result = 0;

            } catch (Exception ex) {
                response.Message = ex.Message;
                response.Result = -1;
                this._log.LogError(ex.ToString());
            }

            return response;
        }
    }
}
