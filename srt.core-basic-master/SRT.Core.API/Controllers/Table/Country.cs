﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Table
{
    [Route("api/[controller]")]
    [ApiController]
    public class Country : ControllerBase
    {
        private readonly ILogger<Services.Table.Country> _log;
        private Services.Table.Contract.ICountry _country;

        public Country(ILogger<Services.Table.Country> log, Services.Table.Contract.ICountry country)
        {
            this._log = log;
            this._country = country;
        }

        [HttpGet]
        public IEnumerable<Services.Table.DTO.Country> Get()
        {
            try
            {
                return this._country.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Table.DTO.Country Get(Guid Id)
        {
            try
            {
                return this._country.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Table.DTO.Country> GetActiveRefObjectState()
        {
            try
            {
                return this._country.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Table.DTO.Country> GetTerminatedRefObjectState()
        {
            try
            {
                return this._country.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Table.DTO.Country> GetByName(string name)
        {
            try
            {
                return this._country.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Table.DTO.Country> ContainsName(string keyword)
        {
            try
            {
                return this._country.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Table.DTO.Country> ContainsAliasName(string keyword)
        {
            try
            {
                return this._country.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Table.DTO.Country> ContainsDescription(string keyword)
        {
            try
            {
                return this._country.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public bool Terminate([FromBody] Services.Table.DTO.Country country, Guid id, Guid updater)
        {
            try
            {
                return this._country.Terminate(id, updater,true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("Reactivate/{id}/{updater}")]
        public bool Reactivate([FromBody] Services.Table.DTO.Country country, Guid id, Guid updater)
        {
            try
            {
                return this._country.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Table.DTO.Country country, Guid updater)
        {
            try
            {
                this._country.Create(country, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Table.DTO.Country> country, Guid updater)
        {
            try
            {
                this._country.CreateInBulk(country, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Table.DTO.Country country, Guid updater)
        {
            try
            {
                this._country.Update(country, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._country.Delete(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }
    }
}
