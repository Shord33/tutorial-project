﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Table
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookUpTable : ControllerBase
    {
        private readonly ILogger<Services.Table.LookUpTable> _log;
        private Services.Table.Contract.ILookUpTable _lookUpTable;

        public LookUpTable(ILogger<Services.Table.LookUpTable> log, Services.Table.Contract.ILookUpTable lookUpTable)
        {
            this._log = log;
            this._lookUpTable = lookUpTable;

        }

        [HttpGet]
        public IEnumerable<Services.Table.DTO.LookUpTable> Get()
        {
            try
            {
                return this._lookUpTable.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Table.DTO.LookUpTable Get(int Id)
        {
            try
            {
                return this._lookUpTable.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Table.DTO.LookUpTable> GetActiveRefObjectState()
        {
            try
            {
                return this._lookUpTable.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Table.DTO.LookUpTable> GetTerminatedRefObjectState()
        {
            try
            {
                return this._lookUpTable.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Table.DTO.LookUpTable> GetByName(string name)
        {
            try
            {
                return this._lookUpTable.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Table.DTO.LookUpTable> ContainsName(string keyword)
        {
            try
            {
                return this._lookUpTable.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Table.DTO.LookUpTable> ContainsAliasName(string keyword)
        {
            try
            {
                return this._lookUpTable.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Table.DTO.LookUpTable> ContainsDescription(string keyword)
        {
            try
            {
                return this._lookUpTable.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public bool Terminate(long id, Guid updater)
        {
            try
            {
                return this._lookUpTable.Terminate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("Reactivate/{id}/{updater}")]
        public bool Reactivate(long id, Guid updater)
        {
            try
            {
                return this._lookUpTable.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Table.DTO.LookUpTable lookUpTable, Guid updater)
        {
            try
            {
                this._lookUpTable.Create(lookUpTable, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Table.DTO.LookUpTable> lookUpTable, Guid updater)
        {
            try
            {
                this._lookUpTable.CreateInBulk(lookUpTable, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Table.DTO.LookUpTable lookUpTable, Guid updater)
        {
            try
            {
                this._lookUpTable.Update(lookUpTable, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(int id, Guid updater)
        {
            try
            {
                this._lookUpTable.Delete(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }
    }
}
