﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Table
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimeZone : ControllerBase
    {
        private readonly ILogger<Services.Table.TimeZone> _log;
        private Services.Table.Contract.ITimeZone _timeZone;

        public TimeZone(ILogger<Services.Table.TimeZone> log, Services.Table.Contract.ITimeZone timeZone)
        {
            this._log = log;
            this._timeZone = timeZone;
        }

        [HttpGet]
        public IEnumerable<Services.Table.DTO.TimeZone> Get()
        {
            try
            {
                return this._timeZone.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Table.DTO.TimeZone Get(Guid Id)
        {
            try
            {
                return this._timeZone.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Table.DTO.TimeZone> GetActiveRefObjectState()
        {
            try
            {
                return this._timeZone.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Table.DTO.TimeZone> GetTerminatedRefObjectState()
        {
            try
            {
                return this._timeZone.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Table.DTO.TimeZone> GetByName(string name)
        {
            try
            {
                return this._timeZone.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Table.DTO.TimeZone> ContainsName(string keyword)
        {
            try
            {
                return this._timeZone.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Table.DTO.TimeZone> ContainsAliasName(string keyword)
        {
            try
            {
                return this._timeZone.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Table.DTO.TimeZone> ContainsDescription(string keyword)
        {
            try
            {
                return this._timeZone.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public bool Terminate([FromBody] Services.Table.DTO.TimeZone timeZone, Guid id, Guid updater)
        {
            try
            {
                return this._timeZone.Terminate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("Reactivate/{id}/{updater}")]
        public bool Reactivate([FromBody] Services.Table.DTO.TimeZone timeZone, Guid id, Guid updater)
        {
            try
            {
                return this._timeZone.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Table.DTO.TimeZone timeZone, Guid updater)
        {
            try
            {
                this._timeZone.Create(timeZone, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Table.DTO.TimeZone> timeZone, Guid updater)
        {
            try
            {
                this._timeZone.CreateInBulk(timeZone, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }


        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Table.DTO.TimeZone timeZone, Guid updater)
        {
            try
            {
                this._timeZone.Update(timeZone, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._timeZone.Delete(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

    }
}
