﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using SRT.Core.Services.Table.Contract;
using SRT.Core.Services.Table.Request;
using SRT.Core.Services.Table.Response;


namespace SRT.Core.API.Controllers.Table
{
    [Route("api/[controller]")]
    [ApiController]
    public class Orchestration : ControllerBase
    {
        private readonly ILogger<Services.Table.LookUpTable> _log;
        //private IUnitOfWork _uow;
        private IOrchestration _orchestration;

        public Orchestration(ILogger<Services.Table.LookUpTable> log, IOrchestration orchestration)
        {
            this._log = log;

            //this._uow = uow;
            this._orchestration = orchestration;
        }

        [HttpPost("GetLookUpTableValueByName")]
        public GetLookupTableValueByNameResponse GetLookUpTableValueByName(GetLookupTableValueByNameRequest request)
        {
            GetLookupTableValueByNameResponse response = new GetLookupTableValueByNameResponse();
            try
            {
                response = this._orchestration.GetLookUpTableValueByName(request);
                response.Result = 0;

            }
            catch (Exception ex)
            {
                response.lookUpTableValues = null;
                response.Message = ex.Message;
                response.Result = -1;
                this._log.LogError(ex.ToString());
            }
            return response;
        }

        [HttpPost("GetLookUpTableValueInKeyPair")]
        public GetLookupTableValueInKeyPairResponse GetLookUpTableValueInKeyPair(GetLookupTableValueInKeyPairRequest request)
        {
            GetLookupTableValueInKeyPairResponse response = new GetLookupTableValueInKeyPairResponse();
            try
            {
                response = this._orchestration.GetLookUpTableValueInKeyPair(request);
                response.Result = 0;

            }
            catch (Exception ex)
            {
                response.keypairValues = null;
                response.Message = ex.Message;
                response.Result = -1;
                this._log.LogError(ex.ToString());
            }
            return response;
        }

        [HttpPost("GetLookUpTableValueByNameAndValue")]
        public GetLookUpTableValueByNameAndValueResponse GetLookUpTableValueByNameAndValue(GetLookUpTableValueByNameAndValueRequest request)
        {
            GetLookUpTableValueByNameAndValueResponse response = new GetLookUpTableValueByNameAndValueResponse();
            try
            {
                response = this._orchestration.GetLookUpTableValueByNameAndValue(request);
                response.Result = 0;

            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Result = -1;
                this._log.LogError(ex.ToString());
            }
            return response;
        }
    }
}
