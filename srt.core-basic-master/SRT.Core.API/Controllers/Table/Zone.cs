﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Table
{
    [Route("api/[controller]")]
    [ApiController]
    public class Zone : ControllerBase
    {
        private readonly ILogger<Services.Table.Zone> _log;
        private Services.Table.Contract.IZone _zone;

        public Zone(ILogger<Services.Table.Zone> log, Services.Table.Contract.IZone zone)
        {
            this._log = log;
            this._zone = zone;
        }

        [HttpGet]
        public IEnumerable<Services.Table.DTO.Zone> Get()
        {
            try
            {
                return this._zone.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Table.DTO.Zone Get(Guid Id)
        {
            try
            {
                return this._zone.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Table.DTO.Zone> GetActiveRefObjectState()
        {
            try
            {
                return this._zone.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Table.DTO.Zone> GetTerminatedRefObjectState()
        {
            try
            {
                return this._zone.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Table.DTO.Zone> GetByName(string name)
        {
            try
            {
                return this._zone.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Table.DTO.Zone> ContainsName(string keyword)
        {
            try
            {
                return this._zone.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Table.DTO.Zone> ContainsAliasName(string keyword)
        {
            try
            {
                return this._zone.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Table.DTO.Zone> ContainsDescription(string keyword)
        {
            try
            {
                return this._zone.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public bool Terminate([FromBody] Services.Table.DTO.Zone zone, Guid id, Guid updater)
        {
            try
            {
                return this._zone.Terminate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("Reactivate/{id}/{updater}")]
        public bool Reactivate([FromBody] Services.Table.DTO.Zone zone, Guid id, Guid updater)
        {
            try
            {
                return this._zone.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Table.DTO.Zone zone, Guid updater)
        {
            try
            {
                this._zone.Create(zone, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Table.DTO.Zone> zone, Guid updater)
        {
            try
            {
                this._zone.CreateInBulk(zone, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Table.DTO.Zone zone, Guid updater)
        {
            try
            {
                this._zone.Update(zone, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._zone.Delete(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

    }
}
