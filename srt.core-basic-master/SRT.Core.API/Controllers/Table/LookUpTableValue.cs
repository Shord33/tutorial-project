﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Table
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookUpTableValue : ControllerBase
    {
        private readonly ILogger<Services.Table.LookUpTableValue> _log;
        private Services.Table.Contract.ILookUpTableValue _lookUpTableValue;

        public LookUpTableValue(ILogger<Services.Table.LookUpTableValue> log, Services.Table.Contract.ILookUpTableValue lookUpTableValue)
        {
            this._log = log;
            this._lookUpTableValue = lookUpTableValue;
        }

        [HttpGet]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> Get()
        {
            try
            {
                return this._lookUpTableValue.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Table.DTO.LookUpTableValue Get(int Id)
        {
            try
            {
                return this._lookUpTableValue.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetActiveRefObjectState()
        {
            try
            {
                return this._lookUpTableValue.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetTerminatedRefObjectState()
        {
            try
            {
                return this._lookUpTableValue.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetByName(string name)
        {
            try
            {
                return this._lookUpTableValue.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> ContainsName(string keyword)
        {
            try
            {
                return this._lookUpTableValue.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> ContainsAliasName(string keyword)
        {
            try
            {
                return this._lookUpTableValue.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsDescription/{keyword}")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> ContainsDescription(string keyword)
        {
            try
            {
                return this._lookUpTableValue.ContainsDescription(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public bool Terminate([FromBody] Services.Table.DTO.LookUpTableValue lookUpTableValue, long id, Guid updater)
        {
            try
            {
                return this._lookUpTableValue.Terminate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("Terminate/{id}/{updater}")]
        public bool Reactivate([FromBody] Services.Table.DTO.LookUpTableValue lookUpTableValue, long id, Guid updater)
        {
            try
            {
                return this._lookUpTableValue.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Table.DTO.LookUpTableValue lookUpTableValue, Guid updater)
        {
            try
            {
                this._lookUpTableValue.Create(lookUpTableValue, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Table.DTO.LookUpTableValue> lookUpTableValue, Guid updater)
        {
            try
            {
                this._lookUpTableValue.CreateInBulk(lookUpTableValue, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Table.DTO.LookUpTableValue lookUpTableValue, Guid updater)
        {
            try
            {
                this._lookUpTableValue.Update(lookUpTableValue, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(int id, Guid updater)
        {
            try
            {
                this._lookUpTableValue.Delete(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetMaintenancePlanType")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetMaintenancePlanType()
        {
            try
            {
                return this._lookUpTableValue.GetMaintenancePlanType();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetMaintenancePlanActivityType")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetMaintenancePlanActivityType()
        {
            try
            {
                return this._lookUpTableValue.GetMaintenancePlanActivityType();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenancePlanActivityExecutionType")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenancePlanActivityExecutionType()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenancePlanActivityExecutionType();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenancePlanActivityCounterType")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenancePlanActivityCounterType()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenancePlanActivityCounterType();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenancePlanActivityCounterEvent")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenancePlanActivityCounterEvent()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenancePlanActivityCounterEvent();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenancePlanActivityAcceptanceMode")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenancePlanActivityAcceptanceMode()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenancePlanActivityAcceptanceMode();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenancePlanActivitReleaseMode")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenancePlanActivitReleaseMode()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenancePlanActivitReleaseMode();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenancePlanActivitBeginCompleteMode")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenancePlanActivitBeginCompleteMode()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenancePlanActivitBeginCompleteMode();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllSchedulerType")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllSchedulerType()
        {
            try
            {
                return this._lookUpTableValue.GetAllSchedulerType();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenanceSchedulerUnit")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenanceSchedulerUnit()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenanceSchedulerUnit();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllSchedulerInterval")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllSchedulerInterval()
        {
            try
            {
                return this._lookUpTableValue.GetAllSchedulerInterval();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllSchedulerMode")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllSchedulerMode()
        {
            try
            {
                return this._lookUpTableValue.GetAllSchedulerMode();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllSchedulerNextMode")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllSchedulerNextMode()
        {
            try
            {
                return this._lookUpTableValue.GetAllSchedulerNextMode();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetAllMaintenanceOrderState")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetAllMaintenanceOrderState()
        {
            try
            {
                return this._lookUpTableValue.GetAllMaintenanceOrderState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }      

        [HttpGet("GetByLookUpTable/{IdLookUpTable}")]
        public IEnumerable<Services.Table.DTO.LookUpTableValue> GetByLookUpTable(long IdLookUpTable)
        {
            try
            {
                return this._lookUpTableValue.GetByLookUpTable(IdLookUpTable);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }
    }
}
