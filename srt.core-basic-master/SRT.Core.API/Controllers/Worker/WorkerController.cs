﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SRT.Core.API.Controllers.Worker
{
    [Route("api/[controller]")]
    [ApiController]
    public class Worker
    {
        private readonly ILogger<Services.Worker.Worker> _log;
        private Services.Worker.Contract.IWorker _worker;

        public Worker(ILogger<Services.Worker.Worker> log, Services.Worker.Contract.IWorker worker)
        {
            this._log = log;
            this._worker = worker;
        }

        [HttpGet]
        public IEnumerable<Services.Worker.DTO.Worker> Get()
        {
            try
            {
                return this._worker.Get();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("{id}")]
        public Services.Worker.DTO.Worker Get(Guid Id)
        {
            try
            {
                return this._worker.GetByKey(Id);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetActiveRefObjectState")]
        public IEnumerable<Services.Worker.DTO.Worker> GetActiveRefObjectState()
        {
            try
            {
                return this._worker.GetActiveRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetTerminatedRefObjectState")]
        public IEnumerable<Services.Worker.DTO.Worker> GetTerminatedRefObjectState()
        {
            try
            {
                return this._worker.GetTerminatedRefObjectState();

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("GetByName/{name}")]
        public IEnumerable<Services.Worker.DTO.Worker> GetbyName(string name)
        {
            try
            {
                return this._worker.GetByName(name);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsName/{keyword}")]
        public IEnumerable<Services.Worker.DTO.Worker> ContainsName(string keyword)
        {
            try
            {
                return this._worker.ContainsName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsAliasName/{keyword}")]
        public IEnumerable<Services.Worker.DTO.Worker> ContainsAliasName(string keyword)
        {
            try
            {
                return this._worker.ContainsAliasName(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet("ContainsPosition/{keyword}")]
        public IEnumerable<Services.Worker.DTO.Worker> ContainsPosition(string keyword)
        {
            try
            {
                return this._worker.ContainsPosition(keyword);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("{updater}")]
        public void Post([FromBody] Services.Worker.DTO.Worker entity, Guid updater)
        {
            try
            {
                this._worker.Create(entity, updater);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost("PostList/{updater}")]
        public void Post([FromBody] IEnumerable<Services.Worker.DTO.Worker> entity, Guid updater)
        {
            try
            {
                this._worker.CreateInBulk(entity, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("{updater}")]
        public void Put([FromBody] Services.Worker.DTO.Worker entity, Guid updater)
        {
            try
            {
                this._worker.Update(entity, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpDelete("{id}/{updater}")]
        public void Delete(Guid id, Guid updater)
        {
            try
            {
                this._worker.Delete(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Terminate/{id}/{updater}")]
        public void Terminate([FromBody] Services.Worker.DTO.Worker entity, Guid id, Guid updater)
        {
            try
            {
                this._worker.Terminate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPut("Reactivate/{id}/{updater}")]
        public void Reactivate([FromBody] Services.Worker.DTO.Worker entity, Guid id, Guid updater)
        {
            try
            {
                this._worker.Reactivate(id, updater, true);

            }
            catch (Exception ex)
            {
                this._log.LogError(ex.ToString());
                throw;
            }
        }

    }
}
