using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Authorization;

using SRT.Core.DAL.UnitOfWork;
using SRT.Core.DAL.Contracts.IUnitOfWork;


using svcArea = SRT.Core.Services.Area;
using svcFactory = SRT.Core.Services.Factory;
using svcWorker = SRT.Core.Services.Worker;

using svcTable = SRT.Core.Services.Table;

using System.Threading;

namespace SRT.Core.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();
           
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            #region Area

            services.AddScoped<svcArea.Contract.IArea, svcArea.Area>();
            services.AddScoped<svcArea.Contract.IAreaAttribute, svcArea.AreaAttribute>();
            services.AddScoped<svcArea.Contract.IOrchestration, svcArea.Orchestration>();

            #endregion

            #region Factory

            services.AddScoped<svcFactory.Contract.IFactory, svcFactory.Factory>();
            services.AddScoped<svcFactory.Contract.IFactoryAttribute, svcFactory.FactoryAttribute>();

            #endregion

            #region Table

            services.AddScoped<svcTable.Contract.IOrchestration, svcTable.Orchestration>();
            services.AddScoped<svcTable.Contract.ICountry, svcTable.Country>();
            services.AddScoped<svcTable.Contract.ILookUpTable, svcTable.LookUpTable>();
            services.AddScoped<svcTable.Contract.ILookUpTableValue, svcTable.LookUpTableValue>();
            services.AddScoped<svcTable.Contract.ITimeZone, svcTable.TimeZone>();
            services.AddScoped<svcTable.Contract.IZone, svcTable.Zone>();

            #endregion

            services.AddScoped<svcWorker.Contract.IWorker, svcWorker.Worker>();


            //services.Configure<RequestLocalizationOptions>(options => {
            //    options.DefaultRequestCulture = new Microsoft.AspNetCore.Localization.RequestCulture("en-US");
            //    options.SupportedCultures = new List<CultureInfo> { new CultureInfo("en-US"), new CultureInfo("en-US") };
            //    options.RequestCultureProviders.Clear();

            //});

            //Get current PC culture
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen();

            services.AddControllers();
            services.AddSwaggerGen();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRequestLocalization();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "SRT Core");

            });
        }
    }
}
