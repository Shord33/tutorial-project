﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IRepository.IFactory;
using SRT.Core.Persistence.Entity;

namespace SRT.Core.DAL.Repository.Factory
{
    public class Factory : BaseRepo<Persistence.Entity.Factory.Factory>, IFactory
    {
        public Factory(SRTCoreEntities dbContext) : base(dbContext)
        {

        }
        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Factory.Factory> GetByRefObjectState(Int64 refObjectState)
        {
            return this.SRTCoreDbContext.Factories.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Factory.Factory> GetByName(string name)
        {
            return this.SRTCoreDbContext.Factories.Where(w => w.Name == name);
        }

        public IEnumerable<Persistence.Entity.Factory.Factory> ContainsName(string name)
        {
            return this.SRTCoreDbContext.Factories.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Factory.Factory> ContainsAliasName(string name)
        {
            return this.SRTCoreDbContext.Factories.Where(w => w.AliasName.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Factory.Factory> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.Factories.Where(w => w.Description.Contains(description));
        }

        public void Insert(Persistence.Entity.Factory.Factory entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Factory.Factory> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Factory.Factory t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Factory.Factory entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Terminate(Persistence.Entity.Factory.Factory factory, Guid updatedBy)
        //{
        //    factory.IdRefObjectState = 0;
        //    factory.IdUserUpdatedBy = updatedBy;
        //    factory.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Factories.Update(factory);
        //}

        //public void Reactivate(Persistence.Entity.Factory.Factory factory, Guid updatedBy)
        //{
        //    factory.IdRefObjectState = 1;
        //    factory.IdUserUpdatedBy = updatedBy;
        //    factory.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Factories.Update(factory);
        //}
    }
}
