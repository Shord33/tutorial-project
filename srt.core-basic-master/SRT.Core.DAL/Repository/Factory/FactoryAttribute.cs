﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IRepository.IFactory;
using SRT.Core.Persistence.Entity;

namespace SRT.Core.DAL.Repository.Factory
{
    public class FactoryAttribute : BaseRepo<Persistence.Entity.Factory.FactoryAttribute>, IFactoryAttribute
    {
        public FactoryAttribute(SRTCoreEntities dbContext) : base(dbContext)
        {

        }
        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Factory.FactoryAttribute> GetByRefObjectState(Int64 refObjectState)
        {
            return this.SRTCoreDbContext.FactoryAttributes.Where(w => w.IdRefObjectState == refObjectState);
        }

        public void Insert(Persistence.Entity.Factory.FactoryAttribute entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Factory.FactoryAttribute> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Factory.FactoryAttribute t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Factory.FactoryAttribute entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Terminate(Persistence.Entity.Factory.FactoryAttribute factoryAttribute, Guid updatedBy)
        //{
        //    factoryAttribute.IdRefObjectState = 0;
        //    factoryAttribute.IdUserUpdatedBy = updatedBy;
        //    factoryAttribute.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.FactoryAttributes.Update(factoryAttribute);
        //}

        //public void Reactivate(Persistence.Entity.Factory.FactoryAttribute factoryAttribute, Guid updatedBy)
        //{
        //    factoryAttribute.IdRefObjectState = 1;
        //    factoryAttribute.IdUserUpdatedBy = updatedBy;
        //    factoryAttribute.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.FactoryAttributes.Update(factoryAttribute);
        //}
    }
}
