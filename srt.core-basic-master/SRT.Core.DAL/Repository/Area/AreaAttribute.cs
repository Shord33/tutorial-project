﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IRepository.IArea;
using SRT.Core.Persistence.Entity;

namespace SRT.Core.DAL.Repository.Area
{
    public class AreaAttribute : BaseRepo<Persistence.Entity.Area.AreaAttribute>, IAreaAttribute
    {
        public AreaAttribute(SRTCoreEntities dbContext) : base(dbContext)
        {

        }
        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Area.AreaAttribute> GetByRefObjectState(Int64 refObjectState)
        {
            return this.SRTCoreDbContext.AreaAttributes.Where(w => w.IdRefObjectState == refObjectState);
        }

        public void Insert(Persistence.Entity.Area.AreaAttribute entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Area.AreaAttribute> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Area.AreaAttribute t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Area.AreaAttribute entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Terminate(Persistence.Entity.Area.AreaAttribute areaAttribute, Guid updatedBy)
        //{
        //    areaAttribute.IdRefObjectState = 0;
        //    areaAttribute.IdUserUpdatedBy = updatedBy;
        //    areaAttribute.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.AreaAttributes.Update(areaAttribute);
        //}

        //public void Reactivate(Persistence.Entity.Area.AreaAttribute areaAttribute, Guid updatedBy)
        //{
        //    areaAttribute.IdRefObjectState = 1;
        //    areaAttribute.IdUserUpdatedBy = updatedBy;
        //    areaAttribute.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.AreaAttributes.Update(areaAttribute);
        //}
    }
}
