﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IRepository.IArea;
using SRT.Core.Persistence.Entity;

namespace SRT.Core.DAL.Repository.Area
{
    public class Area : BaseRepo<Persistence.Entity.Area.Area>, IArea
    {
        public Area(SRTCoreEntities dbContext) : base(dbContext)
        {

        }
        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Area.Area> GetByRefObjectState(Int64 refObjectState)
        {
            return this.SRTCoreDbContext.Areas.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Area.Area> GetByName(string name)
        {
            return this.SRTCoreDbContext.Areas.Where(w => w.Name == name);
        }

        public IEnumerable<Persistence.Entity.Area.Area> ContainsName(string name)
        {
            return this.SRTCoreDbContext.Areas.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Area.Area> ContainsAliasName(string name)
        {
            return this.SRTCoreDbContext.Areas.Where(w => w.AliasName.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Area.Area> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.Areas.Where(w => w.Description.Contains(description));
        }

        public void Insert(Persistence.Entity.Area.Area entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Area.Area> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Area.Area t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Area.Area entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Terminate(Persistence.Entity.Area.Area area, Guid updatedBy)
        //{
        //    area.IdRefObjectState = 0;
        //    area.UpdatedBy = updatedBy;
        //    area.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Areas.Update(area);
        //}

        //public void Reactivate(Persistence.Entity.Area.Area area, Guid updatedBy)
        //{
        //    area.IdRefObjectState = 1;
        //    area.UpdatedBy = updatedBy;
        //    area.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Areas.Update(area);
        //}
    }
}
