﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SRT.Core.DAL.Contracts.IRepository.IWorker;
using SRT.Core.Persistence.Entity;

namespace SRT.Core.DAL.Repository.Worker
{
    public class Worker : BaseRepo<Persistence.Entity.Worker.Worker>, IWorker
    {
        public Worker(SRTCoreEntities dbContext) : base(dbContext)
        {

        }
        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Worker.Worker> GetByRefObjectState(Int64 refObjectState)
        {
            return this.SRTCoreDbContext.Workers.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Worker.Worker> GetByName(string name)
        {
            return this.SRTCoreDbContext.Workers.Where(w => w.Name == name);
        }

        public IEnumerable<Persistence.Entity.Worker.Worker> ContainsName(string name)
        {
            return this.SRTCoreDbContext.Workers.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Worker.Worker> ContainsAliasName(string name)
        {
            return this.SRTCoreDbContext.Workers.Where(w => w.AliasName.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Worker.Worker> ContainsPosition(string position)
        {
            return this.SRTCoreDbContext.Workers.Where(w => w.Position.Contains(position));
        }

        public void Insert(Persistence.Entity.Worker.Worker entity, Guid updatedBy)
        {
            entity.JoinedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Worker.Worker> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Worker.Worker t in entities)
            {
                t.JoinedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Worker.Worker entity, Guid updatedBy)
        {
            entity.JoinedDate = DateTime.UtcNow;
            base.Update(entity);
        }
    }
}
