﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.Contracts.IRepository.ITable;

namespace SRT.Core.DAL.Repository.Table
{
    public class Country : BaseRepo<Persistence.Entity.Table.Country>, ICountry
    {
        public Country(SRTCoreEntities dbContext) : base(dbContext)
        {

        }

        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Table.Country> ContainsAliasName(string aliasName)
        {
            return this.SRTCoreDbContext.Countries.Where(w => w.AliasName.Contains(aliasName));
        }

        public IEnumerable<Persistence.Entity.Table.Country> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.Countries.Where(w => w.Description.Contains(description));
        }

        public IEnumerable<Persistence.Entity.Table.Country> ContainsName(string name)
        {
            return this.SRTCoreDbContext.Countries.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Table.Country> GetByRefObjectState(long refObjectState)
        {
            return this.SRTCoreDbContext.Countries.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Table.Country> GetByName(string name)
        {
            return this.SRTCoreDbContext.Countries.Where(w => w.Name == name);
        }

        public void Insert(Persistence.Entity.Table.Country entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Table.Country> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Table.Country t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Table.Country entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Reactivate(Persistence.Entity.Table.Country country, Guid updatedBy)
        //{
        //    country.IdRefObjectState = 1;
        //    country.IdUserUpdatedBy = updatedBy;
        //    country.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Countries.Update(country);
        //}

        //public void Terminate(Persistence.Entity.Table.Country country, Guid updatedBy)
        //{
        //    country.IdRefObjectState = 0;
        //    country.IdUserUpdatedBy = updatedBy;
        //    country.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Countries.Update(country);
        //}
    }
}
