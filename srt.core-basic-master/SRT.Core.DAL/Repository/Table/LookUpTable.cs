﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.Contracts.IRepository.ITable;


namespace SRT.Core.DAL.Repository.Table
{
    public class LookUpTable : BaseRepo<Persistence.Entity.Table.LookUpTable>, ILookUpTable
    {
        public LookUpTable(SRTCoreEntities dbContext) : base(dbContext)
        {

        }

        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTable> ContainsAliasName(string aliasName)
        {
            return this.SRTCoreDbContext.LookUpTables.Where(w => w.AliasName.Contains(aliasName));
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTable> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.LookUpTables.Where(w => w.Description.Contains(description));
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTable> ContainsName(string name)
        {
            return this.SRTCoreDbContext.LookUpTables.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTable> GetByRefObjectState(long refObjectState)
        {
            return this.SRTCoreDbContext.LookUpTables.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTable> GetByName(string name)
        {
            return this.SRTCoreDbContext.LookUpTables.Where(w => w.Name == name);
        }

        public void Insert(Persistence.Entity.Table.LookUpTable entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Table.LookUpTable> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Table.LookUpTable t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Table.LookUpTable entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Reactivate(Persistence.Entity.Table.LookUpTable lookUpTable, Guid updatedBy)
        //{
        //    lookUpTable.IdRefObjectState = 1;
        //    lookUpTable.IdUserUpdatedBy = updatedBy;
        //    lookUpTable.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.LookUpTables.Update(lookUpTable);
        //}

        //public void Terminate(Persistence.Entity.Table.LookUpTable lookUpTable, Guid updatedBy)
        //{
        //    lookUpTable.IdRefObjectState = 0;
        //    lookUpTable.IdUserUpdatedBy = updatedBy;
        //    lookUpTable.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.LookUpTables.Update(lookUpTable);
        //}
    }
}
