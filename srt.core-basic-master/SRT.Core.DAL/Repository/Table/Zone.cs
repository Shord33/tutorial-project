﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.Contracts.IRepository.ITable;

namespace SRT.Core.DAL.Repository.Table
{
    public class Zone : BaseRepo<Persistence.Entity.Table.Zone>, IZone
    {
        public Zone(SRTCoreEntities dbContext) : base(dbContext)
        {

        }

        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Table.Zone> ContainsAliasName(string aliasName)
        {
            return this.SRTCoreDbContext.Zones.Where(w => w.AliasName.Contains(aliasName));
        }

        public IEnumerable<Persistence.Entity.Table.Zone> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.Zones.Where(w => w.Description.Contains(description));
        }

        public IEnumerable<Persistence.Entity.Table.Zone> ContainsName(string name)
        {
            return this.SRTCoreDbContext.Zones.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Table.Zone> GetByRefObjectState(long refObjectState)
        {
            return this.SRTCoreDbContext.Zones.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Table.Zone> GetByName(string name)
        {
            return this.SRTCoreDbContext.Zones.Where(w => w.Name == name);
        }

        public void Insert(Persistence.Entity.Table.Zone entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Table.Zone> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Table.Zone t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Table.Zone entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Reactivate(Persistence.Entity.Table.Zone zone, Guid updatedBy)
        //{
        //    zone.IdRefObjectState = 1;
        //    zone.IdUserUpdatedBy = updatedBy;
        //    zone.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Zones.Update(zone);
        //}

        //public void Terminate(Persistence.Entity.Table.Zone zone, Guid updatedBy)
        //{
        //    zone.IdRefObjectState = 0;
        //    zone.IdUserUpdatedBy = updatedBy;
        //    zone.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.Zones.Update(zone);
        //}
    }

}
