﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.Contracts.IRepository.ITable;

namespace SRT.Core.DAL.Repository.Table
{
    public class TimeZone : BaseRepo<Persistence.Entity.Table.TimeZone>, ITimeZone
    {
        public TimeZone(SRTCoreEntities dbContext) : base(dbContext)
        {

        }

        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Table.TimeZone> ContainsAliasName(string aliasName)
        {
            return this.SRTCoreDbContext.TimeZones.Where(w => w.AliasName.Contains(aliasName));
        }

        public IEnumerable<Persistence.Entity.Table.TimeZone> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.TimeZones.Where(w => w.Description.Contains(description));
        }

        public IEnumerable<Persistence.Entity.Table.TimeZone> ContainsName(string name)
        {
            return this.SRTCoreDbContext.TimeZones.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Table.TimeZone> GetByRefObjectState(long refObjectState)
        {
            return this.SRTCoreDbContext.TimeZones.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Table.TimeZone> GetByName(string name)
        {
            return this.SRTCoreDbContext.TimeZones.Where(w => w.Name == name);
        }

        public void Insert(Persistence.Entity.Table.TimeZone entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Table.TimeZone> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Table.TimeZone t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Table.TimeZone entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Reactivate(Persistence.Entity.Table.TimeZone timeZone, Guid updatedBy)
        //{
        //    timeZone.IdRefObjectState = 1;
        //    timeZone.IdUserUpdatedBy = updatedBy;
        //    timeZone.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.TimeZones.Update(timeZone);
        //}

        //public void Terminate(Persistence.Entity.Table.TimeZone timeZone, Guid updatedBy)
        //{
        //    timeZone.IdRefObjectState = 0;
        //    timeZone.IdUserUpdatedBy = updatedBy;
        //    timeZone.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.TimeZones.Update(timeZone);
        //}
    }
}
