﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.Contracts.IRepository.ITable;


namespace SRT.Core.DAL.Repository.Table
{
    public class LookUpTableValue : BaseRepo<Persistence.Entity.Table.LookUpTableValue>, ILookUpTableValue
    {
        public LookUpTableValue(SRTCoreEntities dbContext) : base(dbContext)
        {

        }

        public SRTCoreEntities SRTCoreDbContext
        {
            get { return _dbContext as SRTCoreEntities; }
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> ContainsAliasName(string aliasName)
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.AliasName.Contains(aliasName));
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> ContainsDescription(string description)
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.Description.Contains(description));
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> ContainsName(string name)
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.Name.Contains(name));
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetByRefObjectState(long refObjectState)
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.IdRefObjectState == refObjectState);
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetByName(string name)
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.Name == name);
        }       

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetMaintenancePlanType ()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w=>w.LookUpTable1.Name == "Maintenance Plan Type");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetMaintenancePlanActivityType()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w =>  w.LookUpTable1.Name == "Maintenance Plan Activity Type");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenancePlanActivityExecutionType()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Plan Activity Execution Type");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenancePlanActivityCounterType()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Plan Activity Counter Type");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenancePlanActivityCounterEvent()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Plan Activity Counter Event");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenancePlanActivityAcceptanceMode()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Plan Activity Completion Acceptance Mode");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenancePlanActivitReleaseMode()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Plan Activity Order Release Mode");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenancePlanActivitBeginCompleteMode()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Plan Activity Begin and Complete Mode");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllSchedulerType()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Scheduler Type");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenanceSchedulerUnit()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Scheduler Unit");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllSchedulerInterval()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Scheduler Interval");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllSchedulerMode()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Scheduler Mode");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllSchedulerNextMode()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Scheduler Next Mode");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetAllMaintenanceOrderState()
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.LookUpTable1.Name == "Maintenance Order State");
        }

        public IEnumerable<Persistence.Entity.Table.LookUpTableValue> GetByLookUpTable(long IdLookUpTable)
        {
            return this.SRTCoreDbContext.LookUpTableValues.Where(w => w.IdLookUpTable == IdLookUpTable);
        }               

        public void Insert(Persistence.Entity.Table.LookUpTableValue entity, Guid updatedBy)
        {
            entity.IdUserCreatedBy = updatedBy;
            entity.CreatedDate = DateTime.UtcNow;
            base.Insert(entity);
        }

        public void InsertRange(IEnumerable<Persistence.Entity.Table.LookUpTableValue> entities, Guid updatedBy)
        {
            foreach (Persistence.Entity.Table.LookUpTableValue t in entities)
            {
                t.IdUserCreatedBy = updatedBy;
                t.CreatedDate = DateTime.UtcNow;
            }
            base.InsertRange(entities);
        }

        public void Update(Persistence.Entity.Table.LookUpTableValue entity, Guid updatedBy)
        {
            entity.IdUserUpdatedBy = updatedBy;
            entity.UpdatedDate = DateTime.UtcNow;
            base.Update(entity);
        }

        //public void Reactivate(Persistence.Entity.Table.LookUpTableValue lookUpTableValue, Guid updatedBy)
        //{
        //    lookUpTableValue.IdRefObjectState = 1;
        //    lookUpTableValue.IdUserUpdatedBy = updatedBy;
        //    lookUpTableValue.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.LookUpTableValues.Update(lookUpTableValue);
        //}

        //public void Terminate(Persistence.Entity.Table.LookUpTableValue lookUpTableValue, Guid updatedBy)
        //{
        //    lookUpTableValue.IdRefObjectState = 0;
        //    lookUpTableValue.IdUserUpdatedBy = updatedBy;
        //    lookUpTableValue.UpdatedDate = DateTime.UtcNow;

        //    this.SRTCoreDbContext.LookUpTableValues.Update(lookUpTableValue);
        //}
    }
}
