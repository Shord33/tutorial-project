﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using EFCore.BulkExtensions;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.Contracts.IRepository;


namespace SRT.Core.DAL.Repository
{
    public class BaseRepo<TEntity> : IRepo<TEntity> where TEntity : class
    {
        protected readonly SRTCoreEntities _dbContext;
        private DbSet<TEntity> _dbSet;

        public BaseRepo(SRTCoreEntities dbContext)
        {
            this._dbContext = dbContext;
            this._dbSet = dbContext.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = this._dbSet;
            //IQueryable<TEntity> query = this._dbSet.AsNoTracking();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual TEntity GetByPKey(object key)
        {
            TEntity entity  = this._dbSet.Find(key);
            if (entity != null)
            {
                if (this._dbContext.Entry(entity).State != EntityState.Added)
                {
                    this._dbContext.Entry(entity).State = EntityState.Detached;
                }
            }
            return entity;
        }

        public virtual void Insert(TEntity entity)
        {
            this._dbSet.Add(entity);
        }

        public virtual void InsertRange(IEnumerable<TEntity> entities)
        {
            //this._dbSet.AddRange(entities);
            this._dbContext.BulkInsert<TEntity>(entities.ToList());
        }

        public virtual void Delete(object key)
        {
            TEntity entityToDelete = this._dbSet.Find(key);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            //if (this._dbContext.Entry(entityToDelete).State == EntityState.Detached)
            //{
            //    this._dbSet.Attach(entityToDelete);
            //}
            this._dbSet.Remove(entityToDelete);
        }

        public virtual void DeleteRange(IEnumerable<TEntity> entities)
        {
            //this._dbSet.RemoveRange(entities);
            this._dbContext.BulkDelete<TEntity>(entities.ToList());
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            ////if (this._dbContext.Entry(entityToUpdate).State != EntityState.Detached)
            //var local = this._dbSet.Find(key);
            //if (local != null)
            //{
            //    this._dbContext.Entry(local).State = EntityState.Detached;
            //}
            //this._dbSet.Attach(entityToUpdate);
            this._dbContext.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual void UpdateRange(IEnumerable<TEntity> entities)
        {
            this._dbContext.BulkUpdate<TEntity>(entities.ToList());
        }

        public List<T> DataArchiveDBExecuteReader<T>(string query, Func<DbDataReader, T> map)
        {
            //SRTCoreDataArchiveEntities SRTCoreDataArchiveDBContext = new SRTCoreDataArchiveEntities();
            List<T> entities = null;

            //using (var command = SRTCoreDataArchiveDBContext.Database.GetDbConnection().CreateCommand())
            //{
            //    command.CommandText = query;
            //    command.CommandType = CommandType.Text;
            //    if (command.Connection.State == ConnectionState.Closed) command.Connection.Open();

            //    using (var result = command.ExecuteReader())
            //    {
            //        entities = new List<T>();
            //        while (result.Read())
            //        {
            //            entities.Add(map(result));
            //        }
            //    }
            //}
            //SRTCoreDataArchiveDBContext.Dispose();

            return entities;
        }

        public void DataArchiveDBExecuteNonQuery(string query)
        {
            //SRTCoreDataArchiveEntities SRTCoreDataArchiveDBContext = new SRTCoreDataArchiveEntities();
            //using (var command = SRTCoreDataArchiveDBContext.Database.GetDbConnection().CreateCommand())
            //{
            //    command.CommandText = query;
            //    command.CommandType = CommandType.Text;
            //    if (command.Connection.State == ConnectionState.Closed) command.Connection.Open();
            //    command.ExecuteNonQuery();
            //}
            //SRTCoreDataArchiveDBContext.Dispose();
        }
    }
}
