﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using Microsoft.EntityFrameworkCore;

using SRT.Core.Persistence.Entity;

using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.DAL.Contracts.IRepository.ITable;
using SRT.Core.DAL.Contracts.IRepository.IWorker;

using SRT.Core.DAL.Contracts.IRepository.IFactory;
using SRT.Core.DAL.Contracts.IRepository.IArea;

namespace SRT.Core.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private SRTCoreEntities _dbContext;

        public UnitOfWork()
        {
            this._dbContext = new SRTCoreEntities();

            this.LookUpTables = new DAL.Repository.Table.LookUpTable(this._dbContext);
            this.LookUpTableValues = new DAL.Repository.Table.LookUpTableValue(this._dbContext);
            
            this.Countries = new DAL.Repository.Table.Country(this._dbContext);
            this.TimeZones = new DAL.Repository.Table.TimeZone(this._dbContext);
            this.Zones = new DAL.Repository.Table.Zone(this._dbContext);

            this.Factories = new DAL.Repository.Factory.Factory(this._dbContext);
            this.FactoryAttributes = new DAL.Repository.Factory.FactoryAttribute(this._dbContext);
            this.Areas = new DAL.Repository.Area.Area(this._dbContext);
            this.AreaAttributes = new DAL.Repository.Area.AreaAttribute(this._dbContext);
            this.Workers = new DAL.Repository.Worker.Worker(this._dbContext);


        }

        #region Repository Property


        public ILookUpTable LookUpTables { get; private set; }

        public ILookUpTableValue LookUpTableValues { get; private set; }

      
        public ICountry Countries { get; private set; }
        public ITimeZone TimeZones { get; private set; }
        public IZone Zones { get; private set; }
        public IFactory Factories { get; private set; }
        public IArea Areas { get; private set; }
        public IFactoryAttribute FactoryAttributes { get; private set; }
        public IAreaAttribute AreaAttributes { get; private set; }
        public IWorker Workers { get; private set; }

        #endregion


        #region Method


        /// <summary>
        /// Use this method to commit the changes and 
        /// log the Added, Modified & Deleted of Entity State into AuditLog
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int Complete(Guid userId, bool enableChangeTracker = true)
        {
            return this._dbContext.SaveChanges(userId, enableChangeTracker);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this._dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}
