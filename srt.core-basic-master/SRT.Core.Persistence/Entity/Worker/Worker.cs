﻿using System;
using System.Collections.Generic;
using System.Text;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;

namespace SRT.Core.Persistence.Entity.Worker
{
    public class Worker
    {
        public Guid IdWorker { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Position { get; set; }

        public string WorkerCompanyEmail { get; set; }

        public long IdRefObjectState { get; set; }

        public string DbConnectionString { get; set; }

        public DateTime? JoinedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Worker> Workers1 { get; set; }
        public Worker()
        {
            Workers1 = new HashSet<Worker>();
        }

    }
}
