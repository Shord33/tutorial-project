﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using SRT.Core.Persistence.Entity.Table;


namespace SRT.Core.Persistence.Entity.RefObjectState
{
    public class RefObjectState
    {
        public long IdRefObjectState { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public bool IsDefault { get; set; }

        public bool IsDisplay { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }


        #region for relationship

        public User User1 { get; set; }

        public User User2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTable> LookUpTables1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTableValue> LookUpTableValues1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.Country> Countries1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.TimeZone> TimeZones1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.Zone> Zones1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.Factory> Factory1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.Area> Area1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.FactoryAttribute> FactoryAttribute1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.AreaAttribute> AreaAttribute1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Worker.Worker> Worker1 { get; set; }

        #endregion


        public RefObjectState()
        {
            LookUpTables1 = new HashSet<Entity.Table.LookUpTable>();
            LookUpTableValues1 = new HashSet<LookUpTableValue>();

            Users1 = new HashSet<User>();
            Tenants1 = new HashSet<Tenant>();
            Countries1 = new HashSet<Country>();
            TimeZones1 = new HashSet<Table.TimeZone>();
            Zones1 = new HashSet<Table.Zone>();

            Factory1 = new HashSet<Factory.Factory>();
            FactoryAttribute1 = new HashSet<Factory.FactoryAttribute>();

            Area1 = new HashSet<Area.Area>();
            AreaAttribute1 = new HashSet<Area.AreaAttribute>();

            Worker1 = new HashSet<Worker.Worker>();

        }
    }
}
