﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using System.Reflection.Emit;
using Microsoft.Data.SqlClient;

using SRT.Core.Persistence.Entity.Audit;
using SRT.Core.Persistence.EntityConfiguration.Security;
using SRT.Core.Persistence.EntityConfiguration.Table;
using SRT.Core.Persistence.EntityConfiguration.Audit;
using SRT.Core.Persistence.EntityConfiguration.Factory;
using SRT.Core.Persistence.EntityConfiguration.Area;
using SRT.Core.Persistence.EntityConfiguration.RefObjectState;
using SRT.Core.Persistence.EntityConfiguration.Worker;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity
{
    public class SRTCoreEntities : DbContext
    {
        private IConfigurationRoot _appConfig;

        public SRTCoreEntities() 
        {
            IConfigurationBuilder configBuilder = new ConfigurationBuilder()
                                                    .SetBasePath(Directory.GetCurrentDirectory())
                                                    .AddJsonFile("appsettings.json");
            this._appConfig = configBuilder.Build();
            this.ChangeTracker.LazyLoadingEnabled = false;
            this.Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            optionBuilder.UseSqlServer(ConfigurationExtensions.GetConnectionString(this._appConfig, "Default"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new TenantConfiguration());

            modelBuilder.ApplyConfiguration(new RefObjectStateConfiguration());

            modelBuilder.ApplyConfiguration(new AuditLogConfiguration());
            modelBuilder.ApplyConfiguration(new LookUpTableConfiguration());
            modelBuilder.ApplyConfiguration(new LookUpTableValueConfiguration());

            modelBuilder.ApplyConfiguration(new CountryConfiguration());
            modelBuilder.ApplyConfiguration(new TimeZoneConfiguration());
            modelBuilder.ApplyConfiguration(new ZoneConfiguration());

            modelBuilder.ApplyConfiguration(new FactoryConfiguration());
            modelBuilder.ApplyConfiguration(new FactoryAttributeConfiguration());

            modelBuilder.ApplyConfiguration(new AreaConfiguration());
            modelBuilder.ApplyConfiguration(new AreaAttributeConfiguration());

            modelBuilder.ApplyConfiguration(new WorkerConfiguration());
            base.OnModelCreating(modelBuilder);
        }


        #region Audit Log
        public enum AuditActions
        {
            Added,
            Modified,
            Deleted
        }

        ///This is overridden to prevent someone from calling SaveChanges without specifying the user making the change
        public override int SaveChanges()
        {
            throw new InvalidOperationException("User ID must be provided");
        }
        public int SaveChanges(Guid userId, bool enableChangeTracker = true)
        {
            List<AuditLog> auditLogs = new List<AuditLog>();

            if (enableChangeTracker)
            {
                ///Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    ///For each changed record, get the audit record entries and add them
                    foreach (AuditLog x in GetAuditRecordsForChange(ent, userId))
                    {
                        auditLogs.Add(x);
                    }
                }

                this.AuditLogs.AddRange(auditLogs);
            }

            ///Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        private List<AuditLog> GetAuditRecordsForChange(EntityEntry dbEntry, Guid userId)
        {
            List<AuditLog> auditLogs = new List<AuditLog>();

            DateTime updatedDate = DateTime.UtcNow;

            ///Get the Table() attribute, if one exists
            TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), true).SingleOrDefault() as TableAttribute;

            ///Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
            string tableName = tableAttr != null ? tableAttr.Name : dbEntry.Entity.GetType().Name;

            /////Get primary key value (If you have more than one key column, this will need to be adjusted)
            //List<PropertyInfo> keyNames = dbEntry.Entity.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).ToList();

            //string keyName = keyNames[0].Name; //dbEntry.Entity.GetType().GetProperties().Single(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).Name;
            string keyName = dbEntry.Metadata.FindPrimaryKey().Properties.First().Name;


            if (dbEntry.State == EntityState.Added)
            {
                ///For Inserts, just add the whole record
                foreach (IProperty property in dbEntry.CurrentValues.Properties)
                {
                    auditLogs.Add(new AuditLog()
                    {
                        UpdatedBy = userId,
                        UpdatedDate = updatedDate,
                        Action = Enum.Parse(typeof(AuditActions), dbEntry.State.ToString(), true).ToString(),
                        TableName = tableName,
                        //IdRecord = dbEntry.CurrentValues.GetValue<object>(keyName).ToString(),
                        IdRecord = dbEntry.CurrentValues[keyName].ToString(),
                        ColumnName = property.Name,
                        //NewValue = dbEntry.CurrentValues.GetValue<object>(property) == null ? null : dbEntry.CurrentValues.GetValue<object>(property).ToString()
                        NewValue = dbEntry.CurrentValues[property] == null ? null : dbEntry.CurrentValues[property].ToString()
                    }
                    );
                }
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                auditLogs.Add(new AuditLog()
                {
                    UpdatedBy = userId,
                    UpdatedDate = updatedDate,
                    Action = Enum.Parse(typeof(AuditActions), dbEntry.State.ToString(), true).ToString(),
                    TableName = tableName,
                    IdRecord = dbEntry.OriginalValues[keyName].ToString(),
                    ColumnName = keyName,
                    OldValue = dbEntry.OriginalValues[keyName].ToString()
                }
                );
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                foreach (IProperty property in dbEntry.OriginalValues.Properties)
                {
                    ///For updates, we only want to capture the columns that actually changed
                    //if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(property), dbEntry.CurrentValues.GetValue<object>(property)))
                    if (!object.Equals(dbEntry.GetDatabaseValues().GetValue<object>(property), dbEntry.CurrentValues[property]))
                    {
                        auditLogs.Add(new AuditLog()
                        {
                            UpdatedBy = userId,
                            UpdatedDate = updatedDate,
                            Action = Enum.Parse(typeof(AuditActions), dbEntry.State.ToString(), true).ToString(),
                            TableName = tableName,
                            //IdRecord = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                            IdRecord = dbEntry.GetDatabaseValues().GetValue<object>(keyName).ToString(),
                            ColumnName = property.Name,
                            //OldValue = dbEntry.OriginalValues.GetValue<object>(property) == null ? null : dbEntry.OriginalValues.GetValue<object>(property).ToString(),
                            OldValue = dbEntry.GetDatabaseValues().GetValue<object>(property) == null ? null : dbEntry.GetDatabaseValues().GetValue<object>(property).ToString(),
                            //NewValue = dbEntry.CurrentValues.GetValue<object>(property) == null ? null : dbEntry.CurrentValues.GetValue<object>(property).ToString()
                            NewValue = dbEntry.CurrentValues[property] == null ? null : dbEntry.CurrentValues[property].ToString()
                        }
                        );
                    }
                }
            }

            ///Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return auditLogs;
        }

        #endregion


        #region DbSet

        public DbSet<SRT.Core.Persistence.Entity.Security.User> Users { set; get; }
        public DbSet<SRT.Core.Persistence.Entity.Security.Tenant> Tenants { set; get; }

        public DbSet<refObjectState.RefObjectState> ObjectStates { set; get; }

        public DbSet<SRT.Core.Persistence.Entity.Table.LookUpTable> LookUpTables { set; get; }
        public DbSet<SRT.Core.Persistence.Entity.Table.LookUpTableValue> LookUpTableValues { set; get; }
       

        public DbSet<SRT.Core.Persistence.Entity.Table.Country> Countries { set; get; }
        public DbSet<SRT.Core.Persistence.Entity.Table.TimeZone> TimeZones { set; get; }
        public DbSet<SRT.Core.Persistence.Entity.Table.Zone> Zones { set; get; }
        public DbSet<AuditLog> AuditLogs { set; get; }

        
        public DbSet<Factory.Factory> Factories { set; get; }
        public DbSet<Factory.FactoryAttribute> FactoryAttributes { set; get; }
        public DbSet<Area.Area> Areas { set; get; }
        public DbSet<Area.AreaAttribute> AreaAttributes { set; get; }

        public DbSet<SRT.Core.Persistence.Entity.Worker.Worker> Workers { set; get; }

        #endregion
    }
}
