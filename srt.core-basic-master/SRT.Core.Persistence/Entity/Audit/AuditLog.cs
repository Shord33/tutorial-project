﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;




namespace SRT.Core.Persistence.Entity.Audit
{
    public class AuditLog
    {
        public Int64 IdAudit { get; set; }

        public Guid UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string Action { get; set; }

        public string TableName { get; set; }

        public string IdRecord { get; set; }

        public string ColumnName { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

    }
}
