﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity.Area
{
    public class Area
    {
        public Guid IdArea { get; set; }
        public string AreaCoordinate { get; set; }
        public Guid IdFactory { get; set; }
        public string Name { get; set; }
        public string? AliasName { get; set; }
        public string? Description { get; set; }
        public string? ImagePath { get; set; }
        public decimal? XAxis { get; set; }
        public decimal? YAxis { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }
        public Guid? IdAreaParent { get; set; }
        public bool? IsStorageArea { get; set; }


        #region For Relationship

        public User User1 { get; set; }
        public User User2 { get; set; }
        public Tenant Tenant1 { get; set; }
        public refObjectState.RefObjectState RefObjectState1 { get; set; }
        public Area Area1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AreaAttribute> AreaAttributes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area> Areas1 { get; set; }

        #endregion


        public Area()
        {
            AreaAttributes1 = new HashSet<AreaAttribute>();
            Areas1 = new HashSet<Area>();
        }
    }
}
