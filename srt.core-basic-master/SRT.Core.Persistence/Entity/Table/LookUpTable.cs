﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity.Table
{
    public class LookUpTable
    {
        public long IdLookUpTable { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public Guid? IdModule { get; set; }

        public long IdRefObjectState { get; set; }

        public bool IsSystem { get; set; }

        public bool IsConfigurable { get; set; }

        public bool IsEditable { get; set; }
        
        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }


        #region for relationship

        public User User1 { get; set; }

        public User User2 { get; set; }

        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTableValue> LookUpTableValues1 { get; set; }


        #endregion


        public LookUpTable()
        {
            LookUpTableValues1 = new HashSet<LookUpTableValue>();
        }
    }
}
