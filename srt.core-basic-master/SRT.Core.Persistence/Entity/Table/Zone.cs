﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity.Table
{
    public class Zone
    {
        public Guid IdZone { get; set; }

        public Guid IdCountry { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public long IdRefObjectState { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }


        #region for relationship

        public User User1 { get; set; }

        public User User2 { get; set; }

        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        public Country Country1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TimeZone> TimeZones1 { get; set; }

        #endregion

        public Zone()
        {
            TimeZones1 = new HashSet<TimeZone>();
        }
    }
}
