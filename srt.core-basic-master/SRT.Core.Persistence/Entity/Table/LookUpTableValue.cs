﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity.Table
{
    public class LookUpTableValue
    {
        public long IdLookUpTableValue { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public int Value { get; set; }

        public long IdRefObjectState { get; set; }

        public long IdLookUpTable { get; set; }

        public long? IdLookUpTableValueParent { get; set; }

        public bool IsEnable { get; set; }

        public bool IsDefault { get; set; }

        public bool IsDisplay { get; set; }

        public Guid? IdTenant { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }


        #region for relationship

        public LookUpTable LookUpTable1 { get; set; }

        public LookUpTableValue LookUpTableValue1 { get; set; }

        public User User1 { get; set; }

        public User User2 { get; set; }

        public Tenant Tenant1 { get; set; }

        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LookUpTableValue> LookUpTableValues1 { get; set; }

        #endregion


        public LookUpTableValue()
        {
            LookUpTableValues1 = new HashSet<LookUpTableValue>();

        }
    }
}
