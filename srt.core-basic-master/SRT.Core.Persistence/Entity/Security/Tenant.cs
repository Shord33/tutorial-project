﻿using System;
using System.Collections.Generic;
using System.Text;

using refObjectState=SRT.Core.Persistence.Entity.RefObjectState;
using SRT.Core.Persistence.Entity.Table;


namespace SRT.Core.Persistence.Entity.Security
{
    public class Tenant
    {
        public Guid IdTenant { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public string UserRegisteredEmail { get; set; }

        public Guid? IdTenantParent { get; set; }

        public long IdRefObjectState { get; set; }

        public string DbConnectionString { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }


        #region For Relationship

        public Tenant Tenant1 { get; set; }

        public User User1 { get; set; }

        public User User2 { get; set; }

        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LookUpTableValue> LookUpTableValues1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.Factory> Factories1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.Area> Areas1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.FactoryAttribute> FactoryAttributes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.AreaAttribute> AreaAttributes1 { get; set; }

        #endregion


        public Tenant()
        {
            Tenants1 = new HashSet<Tenant>();
            Users1 = new HashSet<User>();
            Users2 = new HashSet<User>();
            LookUpTableValues1 = new HashSet<LookUpTableValue>();

            Factories1 = new HashSet<Factory.Factory>();
            FactoryAttributes1 = new HashSet<Factory.FactoryAttribute>();

            Areas1 = new HashSet<Area.Area>();
            AreaAttributes1 = new HashSet<Area.AreaAttribute>();
        }

    }
}
