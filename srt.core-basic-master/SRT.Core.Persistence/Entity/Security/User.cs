﻿using System;
using System.Collections.Generic;
using System.Text;

using refObjectState=SRT.Core.Persistence.Entity.RefObjectState;
using table = SRT.Core.Persistence.Entity.Table;


namespace SRT.Core.Persistence.Entity.Security
{
    public class User
    {
        public Guid IdUser { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public string NormalizedName { get; set; }

        public long IdRefObjectState { get; set; }

        public bool IsAD { get; set; }

        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string DisplayName { get; set; }

        public string Email { get; set; }

        public string PhoneNo { get; set; }

        public string ProfileImagePath { get; set; }

        public Guid? IdDefaultTenant { get; set; }

        public Guid? CurrentViewingIdTenant { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }


        #region For Relationship

        public User User1 { get; set; }

        public User User2 { get; set; }

        public Tenant Tenant1 { get; set; }

        public Tenant Tenant2 { get; set; }

        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tenant> Tenants2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<refObjectState.RefObjectState> RefObjectStates1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<refObjectState.RefObjectState> RefObjectStates2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTable> LookUpTables1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTable> LookUpTables2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTableValue> LookUpTableValues1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SRT.Core.Persistence.Entity.Table.LookUpTableValue> LookUpTableValues2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<table.Country> Countries1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<table.Country> Countries2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<table.TimeZone> TimeZones1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<table.TimeZone> TimeZones2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<table.Zone> Zones1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<table.Zone> Zones2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.Factory> Factories1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.Factory> Factories2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.Area> Areas1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.Area> Areas2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.FactoryAttribute> FactoryAttributes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Factory.FactoryAttribute> FactoryAttributes2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.AreaAttribute> AreaAttributes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Area.AreaAttribute> AreaAttributes2 { get; set; }

        #endregion


        public User()
        {
            Users1 = new HashSet<User>();
            Users2 = new HashSet<User>();

            Tenants1 = new HashSet<Tenant>();
            Tenants2 = new HashSet<Tenant>();

            RefObjectStates1 = new HashSet<refObjectState.RefObjectState>();
            RefObjectStates2 = new HashSet<refObjectState.RefObjectState>();

            LookUpTables1 = new HashSet<Entity.Table.LookUpTable>();
            LookUpTables2 = new HashSet<Entity.Table.LookUpTable>();

            LookUpTableValues1 = new HashSet<Entity.Table.LookUpTableValue>();
            LookUpTableValues2 = new HashSet<Entity.Table.LookUpTableValue>();

            Countries1 = new HashSet<table.Country>();
            Countries2 = new HashSet<table.Country>();

            TimeZones1 = new HashSet<table.TimeZone>();
            TimeZones2 = new HashSet<table.TimeZone>();

            Zones1 = new HashSet<table.Zone>();
            Zones2 = new HashSet<table.Zone>();

            Factories1 = new HashSet<Factory.Factory>();
            Factories2 = new HashSet<Factory.Factory>();

            Areas1 = new HashSet<Area.Area>();
            Areas2 = new HashSet<Area.Area>();

            FactoryAttributes1 = new HashSet<Factory.FactoryAttribute>();
            FactoryAttributes2 = new HashSet<Factory.FactoryAttribute>();

            AreaAttributes1 = new HashSet<Area.AreaAttribute>();
            AreaAttributes2 = new HashSet<Area.AreaAttribute>();

        }

    }
}
