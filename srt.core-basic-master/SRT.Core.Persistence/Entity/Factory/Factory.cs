﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity.Factory
{
    public class Factory
    {
        public Guid IdFactory { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }


        #region For Relationship
        public User User1 { get; set; }
        public User User2 { get; set; }
        public Tenant Tenant { get; set; }
        public refObjectState.RefObjectState RefObjectState1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FactoryAttribute> FactoryAttributes1 { get; set; }

        #endregion


        public Factory()
        {
            FactoryAttributes1 = new HashSet<FactoryAttribute>();
        }
    }
}
