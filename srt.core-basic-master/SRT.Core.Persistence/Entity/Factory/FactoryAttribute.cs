﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using SRT.Core.Persistence.Entity.Security;
using refObjectState = SRT.Core.Persistence.Entity.RefObjectState;


namespace SRT.Core.Persistence.Entity.Factory
{
    public class FactoryAttribute
    {
        public Guid IdFactoryAttribute { get; set; }
        public Guid IdEntityTypeAttribute { get; set; }
        public string Value { get; set; }
        public Guid IdFactory { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }


        #region For Relationship
        public User User1 { get; set; }
        public User User2 { get; set; }
        public Tenant Tenant1 { get; set; }
        public Factory Factory1 { get; set; }
        public refObjectState.RefObjectState RefObjectState1 { get; set; }
        
        #endregion

        public FactoryAttribute()
        {

        }
    }
}
