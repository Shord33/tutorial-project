﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SRT.Core.Persistence.Migrations
{
    public partial class InitialVersion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Area");

            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.EnsureSchema(
                name: "Factory");

            migrationBuilder.EnsureSchema(
                name: "Security");

            migrationBuilder.CreateTable(
                name: "AuditLog",
                schema: "dbo",
                columns: table => new
                {
                    IdAudit = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpdatedBy = table.Column<Guid>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    Action = table.Column<string>(maxLength: 20, nullable: true),
                    TableName = table.Column<string>(maxLength: 100, nullable: true),
                    IdRecord = table.Column<string>(maxLength: 50, nullable: true),
                    ColumnName = table.Column<string>(maxLength: 100, nullable: true),
                    OldValue = table.Column<string>(type: "Varchar(MAX)", nullable: true),
                    NewValue = table.Column<string>(type: "Varchar(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditLog", x => x.IdAudit);
                });

            migrationBuilder.CreateTable(
                name: "AreaAttribute",
                schema: "Area",
                columns: table => new
                {
                    IdAreaAttribute = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    IdEntityTypeAttribute = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    IdArea = table.Column<Guid>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdTenant = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaAttribute", x => x.IdAreaAttribute);
                });

            migrationBuilder.CreateTable(
                name: "Zone",
                schema: "dbo",
                columns: table => new
                {
                    IdZone = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    IdCountry = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zone", x => x.IdZone);
                });

            migrationBuilder.CreateTable(
                name: "LookUpTableValue",
                schema: "dbo",
                columns: table => new
                {
                    IdLookUpTableValue = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    Value = table.Column<int>(nullable: false),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdLookUpTable = table.Column<long>(nullable: false),
                    IdLookUpTableValueParent = table.Column<long>(nullable: true),
                    IsEnable = table.Column<bool>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    IsDisplay = table.Column<bool>(nullable: false),
                    IdTenant = table.Column<Guid>(nullable: true),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LookUpTableValue", x => x.IdLookUpTableValue);
                    table.ForeignKey(
                        name: "FK_LookUpTableValue_LookUpTableValue_IdLookUpTableValueParent",
                        column: x => x.IdLookUpTableValueParent,
                        principalSchema: "dbo",
                        principalTable: "LookUpTableValue",
                        principalColumn: "IdLookUpTableValue");
                });

            migrationBuilder.CreateTable(
                name: "Area",
                schema: "Area",
                columns: table => new
                {
                    IdArea = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    AreaCoordinate = table.Column<string>(maxLength: 50, nullable: true),
                    IdFactory = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    XAxis = table.Column<decimal>(nullable: true),
                    YAxis = table.Column<decimal>(nullable: true),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdTenant = table.Column<Guid>(nullable: true),
                    IdAreaParent = table.Column<Guid>(nullable: true),
                    IsStorageArea = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.IdArea);
                    table.ForeignKey(
                        name: "FK_Area_Area_IdAreaParent",
                        column: x => x.IdAreaParent,
                        principalSchema: "Area",
                        principalTable: "Area",
                        principalColumn: "IdArea");
                });

            migrationBuilder.CreateTable(
                name: "Country",
                schema: "dbo",
                columns: table => new
                {
                    IdCountry = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    CountryCode = table.Column<string>(maxLength: 2, nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.IdCountry);
                });

            migrationBuilder.CreateTable(
                name: "LookUpTable",
                schema: "dbo",
                columns: table => new
                {
                    IdLookUpTable = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    IdModule = table.Column<Guid>(nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    IsConfigurable = table.Column<bool>(nullable: false),
                    IsEditable = table.Column<bool>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LookUpTable", x => x.IdLookUpTable);
                });

            migrationBuilder.CreateTable(
                name: "TimeZone",
                schema: "dbo",
                columns: table => new
                {
                    IdTimeZone = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    IdZone = table.Column<Guid>(nullable: false),
                    Abbreviation = table.Column<string>(maxLength: 6, nullable: true),
                    TimeStart = table.Column<decimal>(nullable: false),
                    GmtOffset = table.Column<int>(nullable: false),
                    Dst = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeZone", x => x.IdTimeZone);
                    table.ForeignKey(
                        name: "FK_TimeZone_Zone_IdZone",
                        column: x => x.IdZone,
                        principalSchema: "dbo",
                        principalTable: "Zone",
                        principalColumn: "IdZone");
                });

            migrationBuilder.CreateTable(
                name: "Factory",
                schema: "Factory",
                columns: table => new
                {
                    IdFactory = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    ImagePath = table.Column<string>(nullable: true),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdTenant = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Factory", x => x.IdFactory);
                });

            migrationBuilder.CreateTable(
                name: "FactoryAttribute",
                schema: "Factory",
                columns: table => new
                {
                    IdFactoryAttribute = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    IdEntityTypeAttribute = table.Column<Guid>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    IdFactory = table.Column<Guid>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IdTenant = table.Column<Guid>(nullable: true),
                    Factory1IdFactory = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactoryAttribute", x => x.IdFactoryAttribute);
                    table.ForeignKey(
                        name: "FK_FactoryAttribute_Factory_Factory1IdFactory",
                        column: x => x.Factory1IdFactory,
                        principalSchema: "Factory",
                        principalTable: "Factory",
                        principalColumn: "IdFactory",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tenant",
                schema: "Security",
                columns: table => new
                {
                    IdTenant = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    UserRegisteredEmail = table.Column<string>(maxLength: 150, nullable: false),
                    IdTenantParent = table.Column<Guid>(nullable: true),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    DbConnectionString = table.Column<string>(maxLength: 200, nullable: true),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenant", x => x.IdTenant);
                    table.ForeignKey(
                        name: "FK_Tenant_Tenant_IdTenantParent",
                        column: x => x.IdTenantParent,
                        principalSchema: "Security",
                        principalTable: "Tenant",
                        principalColumn: "IdTenant");
                });

            migrationBuilder.CreateTable(
                name: "User",
                schema: "Security",
                columns: table => new
                {
                    IdUser = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 50, nullable: false),
                    IdRefObjectState = table.Column<long>(nullable: false),
                    IsAD = table.Column<bool>(nullable: false),
                    Password = table.Column<string>(maxLength: 150, nullable: true),
                    RememberMe = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    MiddleName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    DisplayName = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    PhoneNo = table.Column<string>(maxLength: 50, nullable: true),
                    ProfileImagePath = table.Column<string>(maxLength: 250, nullable: true),
                    IdDefaultTenant = table.Column<Guid>(nullable: true),
                    CurrentViewingIdTenant = table.Column<Guid>(nullable: true),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.IdUser);
                    table.ForeignKey(
                        name: "FK_User_Tenant_CurrentViewingIdTenant",
                        column: x => x.CurrentViewingIdTenant,
                        principalSchema: "Security",
                        principalTable: "Tenant",
                        principalColumn: "IdTenant");
                    table.ForeignKey(
                        name: "FK_User_Tenant_IdDefaultTenant",
                        column: x => x.IdDefaultTenant,
                        principalSchema: "Security",
                        principalTable: "Tenant",
                        principalColumn: "IdTenant");
                    table.ForeignKey(
                        name: "FK_User_User_IdUserCreatedBy",
                        column: x => x.IdUserCreatedBy,
                        principalSchema: "Security",
                        principalTable: "User",
                        principalColumn: "IdUser");
                    table.ForeignKey(
                        name: "FK_User_User_IdUserUpdatedBy",
                        column: x => x.IdUserUpdatedBy,
                        principalSchema: "Security",
                        principalTable: "User",
                        principalColumn: "IdUser");
                });

            migrationBuilder.CreateTable(
                name: "RefObjectState",
                schema: "dbo",
                columns: table => new
                {
                    IdRefObjectState = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    AliasName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 250, nullable: true),
                    IsDefault = table.Column<bool>(nullable: false),
                    IsDisplay = table.Column<bool>(nullable: false),
                    IdUserCreatedBy = table.Column<Guid>(nullable: true),
                    IdUserUpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefObjectState", x => x.IdRefObjectState);
                    table.ForeignKey(
                        name: "FK_RefObjectState_User_IdUserCreatedBy",
                        column: x => x.IdUserCreatedBy,
                        principalSchema: "Security",
                        principalTable: "User",
                        principalColumn: "IdUser");
                    table.ForeignKey(
                        name: "FK_RefObjectState_User_IdUserUpdatedBy",
                        column: x => x.IdUserUpdatedBy,
                        principalSchema: "Security",
                        principalTable: "User",
                        principalColumn: "IdUser");
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_IdAreaParent",
            //    schema: "Area",
            //    table: "Area",
            //    column: "IdAreaParent");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_IdRefObjectState",
            //    schema: "Area",
            //    table: "Area",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_IdTenant",
            //    schema: "Area",
            //    table: "Area",
            //    column: "IdTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_IdUserCreatedBy",
            //    schema: "Area",
            //    table: "Area",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_IdUserUpdatedBy",
            //    schema: "Area",
            //    table: "Area",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_Name",
            //    schema: "Area",
            //    table: "Area",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_AreaAttribute_IdArea",
            //    schema: "Area",
            //    table: "AreaAttribute",
            //    column: "IdArea");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AreaAttribute_IdRefObjectState",
            //    schema: "Area",
            //    table: "AreaAttribute",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AreaAttribute_IdTenant",
            //    schema: "Area",
            //    table: "AreaAttribute",
            //    column: "IdTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AreaAttribute_IdUserCreatedBy",
            //    schema: "Area",
            //    table: "AreaAttribute",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_AreaAttribute_IdUserUpdatedBy",
            //    schema: "Area",
            //    table: "AreaAttribute",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Country_IdRefObjectState",
            //    schema: "dbo",
            //    table: "Country",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Country_IdUserCreatedBy",
            //    schema: "dbo",
            //    table: "Country",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Country_IdUserUpdatedBy",
            //    schema: "dbo",
            //    table: "Country",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Country_Name",
            //    schema: "dbo",
            //    table: "Country",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTable_IdRefObjectState",
            //    schema: "dbo",
            //    table: "LookUpTable",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTable_IdUserCreatedBy",
            //    schema: "dbo",
            //    table: "LookUpTable",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTable_IdUserUpdatedBy",
            //    schema: "dbo",
            //    table: "LookUpTable",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTable_Name",
            //    schema: "dbo",
            //    table: "LookUpTable",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_IdLookUpTable",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    column: "IdLookUpTable");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_IdLookUpTableValueParent",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    column: "IdLookUpTableValueParent");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_IdRefObjectState",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_IdTenant",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    column: "IdTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_IdUserCreatedBy",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_IdUserUpdatedBy",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_LookUpTableValue_Name_IdTenant",
            //    schema: "dbo",
            //    table: "LookUpTableValue",
            //    columns: new[] { "Name", "IdTenant" },
            //    unique: true,
            //    filter: "[Name] IS NOT NULL AND [IdTenant] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_RefObjectState_IdUserCreatedBy",
            //    schema: "dbo",
            //    table: "RefObjectState",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_RefObjectState_IdUserUpdatedBy",
            //    schema: "dbo",
            //    table: "RefObjectState",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_RefObjectState_Name",
            //    schema: "dbo",
            //    table: "RefObjectState",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_TimeZone_IdRefObjectState",
            //    schema: "dbo",
            //    table: "TimeZone",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TimeZone_IdUserCreatedBy",
            //    schema: "dbo",
            //    table: "TimeZone",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TimeZone_IdUserUpdatedBy",
            //    schema: "dbo",
            //    table: "TimeZone",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TimeZone_IdZone",
            //    schema: "dbo",
            //    table: "TimeZone",
            //    column: "IdZone");

            //migrationBuilder.CreateIndex(
            //    name: "IX_TimeZone_Name",
            //    schema: "dbo",
            //    table: "TimeZone",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Zone_IdCountry",
            //    schema: "dbo",
            //    table: "Zone",
            //    column: "IdCountry");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Zone_IdRefObjectState",
            //    schema: "dbo",
            //    table: "Zone",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Zone_IdUserCreatedBy",
            //    schema: "dbo",
            //    table: "Zone",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Zone_IdUserUpdatedBy",
            //    schema: "dbo",
            //    table: "Zone",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Zone_Name",
            //    schema: "dbo",
            //    table: "Zone",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Factory_IdRefObjectState",
            //    schema: "Factory",
            //    table: "Factory",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Factory_IdTenant",
            //    schema: "Factory",
            //    table: "Factory",
            //    column: "IdTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Factory_IdUserCreatedBy",
            //    schema: "Factory",
            //    table: "Factory",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Factory_IdUserUpdatedBy",
            //    schema: "Factory",
            //    table: "Factory",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Factory_Name",
            //    schema: "Factory",
            //    table: "Factory",
            //    column: "Name",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_FactoryAttribute_Factory1IdFactory",
            //    schema: "Factory",
            //    table: "FactoryAttribute",
            //    column: "Factory1IdFactory");

            //migrationBuilder.CreateIndex(
            //    name: "IX_FactoryAttribute_IdRefObjectState",
            //    schema: "Factory",
            //    table: "FactoryAttribute",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_FactoryAttribute_IdTenant",
            //    schema: "Factory",
            //    table: "FactoryAttribute",
            //    column: "IdTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_FactoryAttribute_IdUserCreatedBy",
            //    schema: "Factory",
            //    table: "FactoryAttribute",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_FactoryAttribute_IdUserUpdatedBy",
            //    schema: "Factory",
            //    table: "FactoryAttribute",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Tenant_IdRefObjectState",
            //    schema: "Security",
            //    table: "Tenant",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Tenant_IdTenantParent",
            //    schema: "Security",
            //    table: "Tenant",
            //    column: "IdTenantParent");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Tenant_IdUserCreatedBy",
            //    schema: "Security",
            //    table: "Tenant",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Tenant_IdUserUpdatedBy",
            //    schema: "Security",
            //    table: "Tenant",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Tenant_Name",
            //    schema: "Security",
            //    table: "Tenant",
            //    column: "Name",
            //    unique: true,
            //    filter: "[Name] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_User_CurrentViewingIdTenant",
            //    schema: "Security",
            //    table: "User",
            //    column: "CurrentViewingIdTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_User_IdDefaultTenant",
            //    schema: "Security",
            //    table: "User",
            //    column: "IdDefaultTenant");

            //migrationBuilder.CreateIndex(
            //    name: "IX_User_IdRefObjectState",
            //    schema: "Security",
            //    table: "User",
            //    column: "IdRefObjectState");

            //migrationBuilder.CreateIndex(
            //    name: "IX_User_IdUserCreatedBy",
            //    schema: "Security",
            //    table: "User",
            //    column: "IdUserCreatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_User_IdUserUpdatedBy",
            //    schema: "Security",
            //    table: "User",
            //    column: "IdUserUpdatedBy");

            //migrationBuilder.CreateIndex(
            //    name: "IX_User_Name",
            //    schema: "Security",
            //    table: "User",
            //    column: "Name",
            //    unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AreaAttribute_RefObjectState_IdRefObjectState",
                schema: "Area",
                table: "AreaAttribute",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaAttribute_Tenant_IdTenant",
                schema: "Area",
                table: "AreaAttribute",
                column: "IdTenant",
                principalSchema: "Security",
                principalTable: "Tenant",
                principalColumn: "IdTenant");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaAttribute_User_IdUserCreatedBy",
                schema: "Area",
                table: "AreaAttribute",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaAttribute_User_IdUserUpdatedBy",
                schema: "Area",
                table: "AreaAttribute",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_AreaAttribute_Area_IdArea",
                schema: "Area",
                table: "AreaAttribute",
                column: "IdArea",
                principalSchema: "Area",
                principalTable: "Area",
                principalColumn: "IdArea");

            migrationBuilder.AddForeignKey(
                name: "FK_Zone_RefObjectState_IdRefObjectState",
                schema: "dbo",
                table: "Zone",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_Zone_User_IdUserCreatedBy",
                schema: "dbo",
                table: "Zone",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Zone_User_IdUserUpdatedBy",
                schema: "dbo",
                table: "Zone",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Zone_Country_IdCountry",
                schema: "dbo",
                table: "Zone",
                column: "IdCountry",
                principalSchema: "dbo",
                principalTable: "Country",
                principalColumn: "IdCountry");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTableValue_RefObjectState_IdRefObjectState",
                schema: "dbo",
                table: "LookUpTableValue",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTableValue_Tenant_IdTenant",
                schema: "dbo",
                table: "LookUpTableValue",
                column: "IdTenant",
                principalSchema: "Security",
                principalTable: "Tenant",
                principalColumn: "IdTenant");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTableValue_User_IdUserCreatedBy",
                schema: "dbo",
                table: "LookUpTableValue",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTableValue_User_IdUserUpdatedBy",
                schema: "dbo",
                table: "LookUpTableValue",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTableValue_LookUpTable_IdLookUpTable",
                schema: "dbo",
                table: "LookUpTableValue",
                column: "IdLookUpTable",
                principalSchema: "dbo",
                principalTable: "LookUpTable",
                principalColumn: "IdLookUpTable");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_RefObjectState_IdRefObjectState",
                schema: "Area",
                table: "Area",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_Tenant_IdTenant",
                schema: "Area",
                table: "Area",
                column: "IdTenant",
                principalSchema: "Security",
                principalTable: "Tenant",
                principalColumn: "IdTenant");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_User_IdUserCreatedBy",
                schema: "Area",
                table: "Area",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_User_IdUserUpdatedBy",
                schema: "Area",
                table: "Area",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Country_RefObjectState_IdRefObjectState",
                schema: "dbo",
                table: "Country",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_Country_User_IdUserCreatedBy",
                schema: "dbo",
                table: "Country",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Country_User_IdUserUpdatedBy",
                schema: "dbo",
                table: "Country",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTable_RefObjectState_IdRefObjectState",
                schema: "dbo",
                table: "LookUpTable",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTable_User_IdUserCreatedBy",
                schema: "dbo",
                table: "LookUpTable",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_LookUpTable_User_IdUserUpdatedBy",
                schema: "dbo",
                table: "LookUpTable",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeZone_RefObjectState_IdRefObjectState",
                schema: "dbo",
                table: "TimeZone",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeZone_User_IdUserCreatedBy",
                schema: "dbo",
                table: "TimeZone",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_TimeZone_User_IdUserUpdatedBy",
                schema: "dbo",
                table: "TimeZone",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Factory_RefObjectState_IdRefObjectState",
                schema: "Factory",
                table: "Factory",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_Factory_Tenant_IdTenant",
                schema: "Factory",
                table: "Factory",
                column: "IdTenant",
                principalSchema: "Security",
                principalTable: "Tenant",
                principalColumn: "IdTenant");

            migrationBuilder.AddForeignKey(
                name: "FK_Factory_User_IdUserCreatedBy",
                schema: "Factory",
                table: "Factory",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Factory_User_IdUserUpdatedBy",
                schema: "Factory",
                table: "Factory",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_FactoryAttribute_RefObjectState_IdRefObjectState",
                schema: "Factory",
                table: "FactoryAttribute",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_FactoryAttribute_Tenant_IdTenant",
                schema: "Factory",
                table: "FactoryAttribute",
                column: "IdTenant",
                principalSchema: "Security",
                principalTable: "Tenant",
                principalColumn: "IdTenant");

            migrationBuilder.AddForeignKey(
                name: "FK_FactoryAttribute_User_IdUserCreatedBy",
                schema: "Factory",
                table: "FactoryAttribute",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_FactoryAttribute_User_IdUserUpdatedBy",
                schema: "Factory",
                table: "FactoryAttribute",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Tenant_RefObjectState_IdRefObjectState",
                schema: "Security",
                table: "Tenant",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");

            migrationBuilder.AddForeignKey(
                name: "FK_Tenant_User_IdUserCreatedBy",
                schema: "Security",
                table: "Tenant",
                column: "IdUserCreatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_Tenant_User_IdUserUpdatedBy",
                schema: "Security",
                table: "Tenant",
                column: "IdUserUpdatedBy",
                principalSchema: "Security",
                principalTable: "User",
                principalColumn: "IdUser");

            migrationBuilder.AddForeignKey(
                name: "FK_User_RefObjectState_IdRefObjectState",
                schema: "Security",
                table: "User",
                column: "IdRefObjectState",
                principalSchema: "dbo",
                principalTable: "RefObjectState",
                principalColumn: "IdRefObjectState");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tenant_RefObjectState_IdRefObjectState",
                schema: "Security",
                table: "Tenant");

            migrationBuilder.DropForeignKey(
                name: "FK_User_RefObjectState_IdRefObjectState",
                schema: "Security",
                table: "User");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Tenant_CurrentViewingIdTenant",
                schema: "Security",
                table: "User");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Tenant_IdDefaultTenant",
                schema: "Security",
                table: "User");

            migrationBuilder.DropTable(
                name: "AreaAttribute",
                schema: "Area");

            migrationBuilder.DropTable(
                name: "AuditLog",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "LookUpTableValue",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "TimeZone",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "FactoryAttribute",
                schema: "Factory");

            migrationBuilder.DropTable(
                name: "Area",
                schema: "Area");

            migrationBuilder.DropTable(
                name: "LookUpTable",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Zone",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Factory",
                schema: "Factory");

            migrationBuilder.DropTable(
                name: "Country",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "RefObjectState",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Tenant",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "User",
                schema: "Security");
        }
    }
}
