﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Persistence.EntityConfiguration
{
    internal class SchemaConstant
    {
        public const string DefaultDbSchema = "dbo";
        public const string SecurityDbSchema = "Security";
        public const string MaintenanceDbSchema = "Maintenance";
        public const string TaskDbSchema = "Task";
        public const string ResourceDbSchema = "Resource";
        public const string EntityTypeDbSchema = "EntityType";
        public const string FactoryDbSchema = "Factory";
        public const string WorkerDbSchema = "Worker";
        public const string AreaDbSchema = "Area";
        public const string AlarmDbSchema = "Alarm";
        public const string NotificationDbSchema = "Notification";
        public const string BusinessIntelligenceDbSchema = "BusinessIntelligence";
        public const string ParameterDbSchema = "Parameter";
        public const string DataCollectionDbSchema = "DataCollection";
        public const string IOTagSchema = "IOTag";
        public const string RecipeSchema = "Recipe";
        public const string RuleDbSchema = "Rule";
        public const string ProductionDbSchema = "Production";
        public const string DashboarDbSchema = "Dashboard";
        public const string ConfigurationDbSchema = "Configuration";
    }
}
