﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SRT.Core.Persistence.EntityConfiguration.Factory
{
    class FactoryConfiguration : IEntityTypeConfiguration<SRT.Core.Persistence.Entity.Factory.Factory>
    {
        public void Configure(EntityTypeBuilder<SRT.Core.Persistence.Entity.Factory.Factory> builder)
        {
            #region Table Override

            ///Define table schema
            builder.ToTable("Factory", SchemaConstant.FactoryDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdFactory);

            /// Define unique constraint
            builder.HasIndex(k => new { k.Name }).IsUnique();

            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdFactory)
                .HasDefaultValueSql("NEWID()");

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.AliasName)
                .HasMaxLength(50);

            builder.Property(p => p.Description)
                .HasMaxLength(250);

            builder.Property(p => p.IdFactory)
                .IsRequired();

            #endregion

            #region Relationship

            //Configure relationship

            builder.HasOne(r => r.User1)
                .WithMany(r => r.Factories1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.Factories2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Tenant)
                .WithMany(r => r.Factories1)
                .HasForeignKey(k => k.IdTenant)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.Factory1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            #endregion
        }
    }
}
