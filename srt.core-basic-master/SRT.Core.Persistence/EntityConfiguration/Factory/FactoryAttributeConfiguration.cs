﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SRT.Core.Persistence.EntityConfiguration.Factory
{
    class FactoryAttributeConfiguration : IEntityTypeConfiguration<SRT.Core.Persistence.Entity.Factory.FactoryAttribute>
    {
        public void Configure(EntityTypeBuilder<SRT.Core.Persistence.Entity.Factory.FactoryAttribute> builder)
        {
            #region Table Override

            ///Define table schema
            builder.ToTable("FactoryAttribute", SchemaConstant.FactoryDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdFactoryAttribute);

            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdFactoryAttribute)
                .HasDefaultValueSql("NEWID()");

            builder.Property(p => p.IdFactoryAttribute)
                .IsRequired();

            builder.Property(p => p.IdFactory)
                .IsRequired();

            builder.Property(p => p.IdEntityTypeAttribute)
                .IsRequired();
            #endregion

            #region Relationship

            //Configure relationship

            builder.HasOne(r => r.User1)
                .WithMany(r => r.FactoryAttributes1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.FactoryAttributes2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Tenant1)
                .WithMany(r => r.FactoryAttributes1)
                .HasForeignKey(k => k.IdTenant)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.FactoryAttribute1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            #endregion
        }
    }
}
