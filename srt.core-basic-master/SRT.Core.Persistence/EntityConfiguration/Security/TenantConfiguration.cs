﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using SRT.Core.Persistence.Entity.Security;


namespace SRT.Core.Persistence.EntityConfiguration.Security
{
    class TenantConfiguration : IEntityTypeConfiguration<Tenant>
    {
        public void Configure(EntityTypeBuilder<Tenant> builder)
        {
            #region Table Override

            ///Define table schema
            builder.ToTable("Tenant", SchemaConstant.SecurityDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdTenant);

            ///Define unique constraint
            builder.HasIndex(k => k.Name).IsUnique();

            #endregion


            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdTenant)
                .HasDefaultValueSql("NEWID()");

            builder.Property(p => p.Name)
                .HasMaxLength(50);

            builder.Property(p => p.AliasName)
                .HasMaxLength(50);

            builder.Property(p => p.Description)
                .HasMaxLength(250);

            builder.Property(p => p.UserRegisteredEmail)
                .IsRequired()
                .HasMaxLength(150);

            builder.Property(p => p.DbConnectionString)
                .HasMaxLength(200);

            #endregion


            #region Relationship

            ///Configure the relationship

            builder.HasOne(r => r.Tenant1)
                .WithMany(r => r.Tenants1)
                .HasForeignKey(k => k.IdTenantParent)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User1)
                .WithMany(r => r.Tenants1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.Tenants2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.Tenants1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            #endregion

        }
    }
}
