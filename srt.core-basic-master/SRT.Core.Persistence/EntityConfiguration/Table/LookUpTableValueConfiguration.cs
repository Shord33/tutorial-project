﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using SRT.Core.Persistence.Entity.Table;



namespace SRT.Core.Persistence.EntityConfiguration.Table
{
    class LookUpTableValueConfiguration : IEntityTypeConfiguration<LookUpTableValue>
    {
        public void Configure(EntityTypeBuilder<LookUpTableValue> builder)
        {
            #region Table Override

            ///Define table schema
            //builder.ToTable(builder.Metadata.GetTableName(), SchemaConstant.SecurityDbSchema);
            builder.ToTable("LookUpTableValue", SchemaConstant.DefaultDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdLookUpTableValue);

            ///Define unique constraint
            builder.HasIndex(k => new { k.Name, k.IdTenant }).IsUnique();

            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdLookUpTableValue)
                .ValueGeneratedOnAdd();

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.AliasName)
                .HasMaxLength(50);

            builder.Property(p => p.Description)
                .HasMaxLength(250);

            builder.Property(p => p.Value)
                .IsRequired();

            builder.Property(p => p.IdLookUpTable)
                .IsRequired();

            builder.Property(p => p.IsEnable)
               .IsRequired();

            builder.Property(p => p.IsDefault)
               .IsRequired();

            builder.Property(p => p.IsDisplay)
               .IsRequired();


            #endregion

            #region Relationship

            ///Configure the relationship

            builder.HasOne(r => r.User1)
                .WithMany(r => r.LookUpTableValues1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.LookUpTableValues2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Tenant1)
                .WithMany(r => r.LookUpTableValues1)
                .HasForeignKey(k => k.IdTenant)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.LookUpTableValues1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.LookUpTable1)
                .WithMany(r => r.LookUpTableValues1)
                .HasForeignKey(k => k.IdLookUpTable)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.LookUpTableValue1)
               .WithMany(r => r.LookUpTableValues1)
               .HasForeignKey(k => k.IdLookUpTableValueParent)
               .OnDelete(DeleteBehavior.NoAction);

            #endregion

        }
    }
}
