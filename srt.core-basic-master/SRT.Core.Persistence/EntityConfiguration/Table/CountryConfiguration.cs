﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using SRT.Core.Persistence.Entity.Table;


namespace SRT.Core.Persistence.EntityConfiguration.Table
{
    class CountryConfiguration : IEntityTypeConfiguration<Entity.Table.Country>
    {
        public void Configure(EntityTypeBuilder<Entity.Table.Country> builder)
        {
            #region Table Override

            ///Define table schema
            //builder.ToTable(builder.Metadata.GetTableName(), SchemaConstant.SecurityDbSchema);
            builder.ToTable("Country", SchemaConstant.DefaultDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdCountry);

            ///Define unique constraint
            builder.HasIndex(k => k.Name).IsUnique();

            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdCountry)
               .HasDefaultValueSql("NEWID()");

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.AliasName)
                .HasMaxLength(50);

            builder.Property(p => p.Description)
                .HasMaxLength(250);

            builder.Property(p => p.CountryCode)
               .HasMaxLength(2);

            #endregion

            #region Relationship

            ///Configure the relationship

            builder.HasOne(r => r.User1)
                .WithMany(r => r.Countries1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.Countries2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.Countries1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            #endregion
        }
    }
}
