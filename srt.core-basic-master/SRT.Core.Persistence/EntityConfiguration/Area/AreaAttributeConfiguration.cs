﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SRT.Core.Persistence.EntityConfiguration.Area
{
    class AreaAttributeConfiguration : IEntityTypeConfiguration<SRT.Core.Persistence.Entity.Area.AreaAttribute>
    {
        public void Configure(EntityTypeBuilder<SRT.Core.Persistence.Entity.Area.AreaAttribute> builder)
        {
            #region Table Override

            ///Define table schema
            builder.ToTable("AreaAttribute", SchemaConstant.AreaDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdAreaAttribute);

            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdAreaAttribute)
                .HasDefaultValueSql("NEWID()");

            builder.Property(p => p.IdAreaAttribute)
                .IsRequired();

            builder.Property(p => p.IdArea)
                .IsRequired();

            builder.Property(p => p.IdEntityTypeAttribute)
                .IsRequired();
            #endregion

            #region Relationship

            //Configure relationship

            builder.HasOne(r => r.User1)
                .WithMany(r => r.AreaAttributes1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.AreaAttributes2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Tenant1)
                .WithMany(r => r.AreaAttributes1)
                .HasForeignKey(k => k.IdTenant)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.AreaAttribute1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Area1)
                .WithMany(r => r.AreaAttributes1)
                .HasForeignKey(k => k.IdArea)
                .OnDelete(DeleteBehavior.NoAction);

            #endregion
        }
    }
}
