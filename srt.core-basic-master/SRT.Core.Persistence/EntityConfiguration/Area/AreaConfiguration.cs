﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SRT.Core.Persistence.EntityConfiguration.Area
{
    class AreaConfiguration : IEntityTypeConfiguration<SRT.Core.Persistence.Entity.Area.Area>
    {
        public void Configure(EntityTypeBuilder<SRT.Core.Persistence.Entity.Area.Area> builder)
        {
            #region Table Override

            ///Define table schema
            builder.ToTable("Area", SchemaConstant.AreaDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdArea);

            ///Define unique constraint
            builder.HasIndex(k => new { k.Name }).IsUnique();


            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdArea)
                .HasDefaultValueSql("NEWID()");
           
            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.AliasName)
                .HasMaxLength(50);

            builder.Property(p => p.Description)
                .HasMaxLength(250);

            builder.Property(p => p.IdFactory)
                .IsRequired();

            builder.Property(p => p.IdArea)
                .IsRequired();

            builder.Property(p => p.AreaCoordinate)
                .IsRequired(false)
                .HasMaxLength(50);

            #endregion

            #region Relationship

            //Configure relationship

            builder.HasOne(r => r.User1)
                .WithMany(r => r.Areas1)
                .HasForeignKey(k => k.IdUserCreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.User2)
                .WithMany(r => r.Areas2)
                .HasForeignKey(k => k.IdUserUpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Tenant1)
                .WithMany(r => r.Areas1)
                .HasForeignKey(k => k.IdTenant)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.RefObjectState1)
                .WithMany(r => r.Area1)
                .HasForeignKey(k => k.IdRefObjectState)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.Area1)
                .WithMany(r => r.Areas1)
                .HasForeignKey(k => k.IdAreaParent)
                .OnDelete(DeleteBehavior.NoAction);

            #endregion
        }
    }
}
