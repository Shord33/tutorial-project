﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SRT.Core.Persistence.EntityConfiguration.Worker
{
    class WorkerConfiguration : IEntityTypeConfiguration<SRT.Core.Persistence.Entity.Worker.Worker>
    {
        public void Configure(EntityTypeBuilder<SRT.Core.Persistence.Entity.Worker.Worker> builder)
        {
            #region Table Override

            ///Define table schema
            builder.ToTable("Worker", SchemaConstant.WorkerDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdWorker);

            ///Define unique constraint
            builder.HasIndex(k => new { k.Name }).IsUnique();


            #endregion

            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdWorker)
                .HasDefaultValueSql("NEWID()");

            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(p => p.AliasName)
                .HasMaxLength(50);

            builder.Property(p => p.Position)
                .HasMaxLength(50);

            builder.Property(p => p.WorkerCompanyEmail)
                .HasMaxLength(50);
     

            #endregion

          
        }
    }
}

