﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using SRT.Core.Persistence.Entity.Audit;


namespace SRT.Core.Persistence.EntityConfiguration.Audit
{
    class AuditLogConfiguration : IEntityTypeConfiguration<AuditLog>
    {
        public void Configure(EntityTypeBuilder<AuditLog> builder)
        {
            #region Table Override

            ///Define table schema
            //builder.ToTable(builder.Metadata.GetTableName(), SchemaConstant.DefaultDbSchema);
            builder.ToTable("AuditLog", SchemaConstant.DefaultDbSchema);

            ///Define primary key
            builder.HasKey(k => k.IdAudit);

            #endregion


            #region Property Configuration

            ///Set auto generate value for primary key
            builder.Property(p => p.IdAudit)
                .ValueGeneratedOnAdd();

            builder.Property(p => p.UpdatedBy)
                .IsRequired();

            builder.Property(p => p.Action)
                .HasMaxLength(20);

            builder.Property(p => p.TableName)
                .HasMaxLength(100);

            builder.Property(p => p.IdRecord)
                .HasMaxLength(50);

            builder.Property(p => p.ColumnName)
                .HasMaxLength(100);

            builder.Property(p => p.OldValue)
                .HasColumnType("Varchar(MAX)");

            builder.Property(p => p.NewValue)
                .HasColumnType("Varchar(MAX)");

            #endregion


            #region Relationship

            ///Configure the relationship


            #endregion

        }
    }
}
