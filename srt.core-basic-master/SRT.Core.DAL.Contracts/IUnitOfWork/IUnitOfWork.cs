﻿using System;
using System.Collections.Generic;
using System.Text;


using SRT.Core.DAL.Contracts.IRepository.ITable;
using SRT.Core.DAL.Contracts.IRepository.IWorker;
using SRT.Core.DAL.Contracts.IRepository.IFactory;
using SRT.Core.DAL.Contracts.IRepository.IArea;

namespace SRT.Core.DAL.Contracts.IUnitOfWork
{
    public interface IUnitOfWork
    {
        #region Repository Property

        ILookUpTable LookUpTables { get; }

        ILookUpTableValue LookUpTableValues { get; }

        ICountry Countries { get; }

        ITimeZone TimeZones { get; }

        IZone Zones { get; }

        

        IFactory Factories { get; }

        IArea Areas { get; }

        IFactoryAttribute FactoryAttributes { get; }

        IAreaAttribute AreaAttributes { get; }

        IWorker Workers { get; }

        #endregion


        #region Method


        int Complete(Guid userId, bool enableChangeTracker = true);

        void Dispose();

        #endregion

    }
}
