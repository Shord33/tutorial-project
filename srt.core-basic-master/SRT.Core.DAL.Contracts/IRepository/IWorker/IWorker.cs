﻿using System;
using System.Collections.Generic;
using SRT.Core.Persistence.Entity.Worker;
using System.Text;

namespace SRT.Core.DAL.Contracts.IRepository.IWorker
{
    public interface IWorker : IRepo<Worker>
    {
        IEnumerable<Worker> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<Worker> GetByName(string name);
        IEnumerable<Worker> ContainsName(string name);
        IEnumerable<Worker> ContainsAliasName(string name);
        IEnumerable<Worker> ContainsPosition(string position);
        void Insert(Persistence.Entity.Worker.Worker entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Worker.Worker> entities, Guid updatedBy);
        void Update(Persistence.Entity.Worker.Worker entity, Guid updatedBy);

    }
}
