﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Area;

namespace SRT.Core.DAL.Contracts.IRepository.IArea
{
    public interface IArea : IRepo<Area>
    {
        IEnumerable<Area> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<Area> GetByName(string name);
        IEnumerable<Area> ContainsName(string name);
        IEnumerable<Area> ContainsAliasName(string name);
        IEnumerable<Area> ContainsDescription(string description);
        void Insert(Persistence.Entity.Area.Area entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Area.Area> entities, Guid updatedBy);
        void Update(Persistence.Entity.Area.Area entity, Guid updatedBy);

        //void Terminate(Area area, Guid updatedBy);
        //void Reactivate(Area area, Guid updatedBy);
    }
}