﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Area;

namespace SRT.Core.DAL.Contracts.IRepository.IArea
{
    public interface IAreaAttribute : IRepo<AreaAttribute>
    {
        IEnumerable<AreaAttribute> GetByRefObjectState(Int64 refObjectState);
        void Insert(Persistence.Entity.Area.AreaAttribute entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Area.AreaAttribute> entities, Guid updatedBy);
        void Update(Persistence.Entity.Area.AreaAttribute entity, Guid updatedBy);

        //void Terminate(AreaAttribute areaAttribute, Guid updatedBy);
        //void Reactivate(AreaAttribute areaAttribute, Guid updatedBy);
    }
}