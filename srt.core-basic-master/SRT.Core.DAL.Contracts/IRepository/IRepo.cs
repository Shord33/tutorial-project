﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;


namespace SRT.Core.DAL.Contracts.IRepository
{
    public interface IRepo<TEntity> where TEntity : class
    {
        void Delete(TEntity entityToDelete);

        void Delete(object key);

        void DeleteRange(IEnumerable<TEntity> entities);

        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        TEntity GetByPKey(object key);

        void Insert(TEntity entity);

        void InsertRange(IEnumerable<TEntity> entities);

        void Update(TEntity entityToUpdate);
    }

}
