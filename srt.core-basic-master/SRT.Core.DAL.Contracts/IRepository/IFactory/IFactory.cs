﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Factory;

namespace SRT.Core.DAL.Contracts.IRepository.IFactory
{
    public interface IFactory : IRepo<Factory>
    {
        IEnumerable<Factory> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<Factory> GetByName(string name);
        IEnumerable<Factory> ContainsName(string name);
        IEnumerable<Factory> ContainsAliasName(string name);
        IEnumerable<Factory> ContainsDescription(string description);
        void Insert(Persistence.Entity.Factory.Factory entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Factory.Factory> entities, Guid updatedBy);
        void Update(Persistence.Entity.Factory.Factory entity, Guid updatedBy);

        //void Terminate(Factory factory, Guid updatedBy);
        //void Reactivate(Factory factory, Guid updatedBy);
    }
}
