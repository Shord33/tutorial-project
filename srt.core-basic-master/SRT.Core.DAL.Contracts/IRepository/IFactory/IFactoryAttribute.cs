﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Factory;

namespace SRT.Core.DAL.Contracts.IRepository.IFactory
{
    public interface IFactoryAttribute : IRepo<FactoryAttribute>
    {
        IEnumerable<FactoryAttribute> GetByRefObjectState(Int64 refObjectState);
        void Insert(Persistence.Entity.Factory.FactoryAttribute entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Factory.FactoryAttribute> entities, Guid updatedBy);
        void Update(Persistence.Entity.Factory.FactoryAttribute entity, Guid updatedBy);

        //void Terminate(FactoryAttribute factoryAttribute, Guid updatedBy);
        //void Reactivate(FactoryAttribute factoryAttribute, Guid updatedBy);
    }
}
