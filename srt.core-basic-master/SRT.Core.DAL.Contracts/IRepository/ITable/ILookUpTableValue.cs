﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Table;


namespace SRT.Core.DAL.Contracts.IRepository.ITable
{
    public interface ILookUpTableValue : IRepo<LookUpTableValue>
    {
        IEnumerable<LookUpTableValue> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<LookUpTableValue> GetByName(string name);       
        IEnumerable<LookUpTableValue> ContainsName(string name);
        IEnumerable<LookUpTableValue> ContainsAliasName(string aliasName);
        IEnumerable<LookUpTableValue> ContainsDescription(string description);
        IEnumerable<LookUpTableValue> GetMaintenancePlanType ();
        IEnumerable<LookUpTableValue> GetMaintenancePlanActivityType();
        IEnumerable<LookUpTableValue> GetAllMaintenancePlanActivityExecutionType();
        IEnumerable<LookUpTableValue> GetAllMaintenancePlanActivityCounterType();
        IEnumerable<LookUpTableValue> GetAllMaintenancePlanActivityCounterEvent();
        IEnumerable<LookUpTableValue> GetAllMaintenancePlanActivityAcceptanceMode();
        IEnumerable<LookUpTableValue> GetAllMaintenancePlanActivitReleaseMode();
        IEnumerable<LookUpTableValue> GetAllMaintenancePlanActivitBeginCompleteMode();
        IEnumerable<LookUpTableValue> GetAllSchedulerType();
        IEnumerable<LookUpTableValue> GetAllMaintenanceSchedulerUnit();
        IEnumerable<LookUpTableValue> GetAllSchedulerInterval();
        IEnumerable<LookUpTableValue> GetAllSchedulerMode();
        IEnumerable<LookUpTableValue> GetAllSchedulerNextMode();
        IEnumerable<LookUpTableValue> GetAllMaintenanceOrderState();
        IEnumerable<LookUpTableValue> GetByLookUpTable(long IdLookUpTable);          
        void Insert(Persistence.Entity.Table.LookUpTableValue entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Table.LookUpTableValue> entities, Guid updatedBy);
        void Update(Persistence.Entity.Table.LookUpTableValue entity, Guid updatedBy);

        //void Terminate(LookUpTableValue lookUpTableValue, Guid updatedBy);
        //void Reactivate(LookUpTableValue lookUpTableValue, Guid updatedBy);
    }
}
