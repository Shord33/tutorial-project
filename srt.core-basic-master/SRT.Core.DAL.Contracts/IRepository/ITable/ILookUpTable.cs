﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Table;


namespace SRT.Core.DAL.Contracts.IRepository.ITable
{
    public interface ILookUpTable : IRepo<LookUpTable>
    {
        IEnumerable<LookUpTable> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<LookUpTable> GetByName(string name);
        IEnumerable<LookUpTable> ContainsName(string name);
        IEnumerable<LookUpTable> ContainsAliasName(string aliasName);
        IEnumerable<LookUpTable> ContainsDescription(string description);
        void Insert(Persistence.Entity.Table.LookUpTable entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Table.LookUpTable> entities, Guid updatedBy);
        void Update(Persistence.Entity.Table.LookUpTable entity, Guid updatedBy);

        //public void Terminate(LookUpTable lookUpTable, Guid updatedBy);
        //public void Reactivate(LookUpTable lookUpTable, Guid updatedBy);
    }
}
