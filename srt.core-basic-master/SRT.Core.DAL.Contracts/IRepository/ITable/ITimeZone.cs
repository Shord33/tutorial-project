﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Table;

namespace SRT.Core.DAL.Contracts.IRepository.ITable
{
    public interface ITimeZone : IRepo<SRT.Core.Persistence.Entity.Table.TimeZone>
    {
        IEnumerable<Persistence.Entity.Table.TimeZone> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<Persistence.Entity.Table.TimeZone> GetByName(string name);
        IEnumerable<Persistence.Entity.Table.TimeZone> ContainsName(string name);
        IEnumerable<Persistence.Entity.Table.TimeZone> ContainsAliasName(string aliasName);
        IEnumerable<Persistence.Entity.Table.TimeZone> ContainsDescription(string description);
        void Insert(Persistence.Entity.Table.TimeZone entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Table.TimeZone> entities, Guid updatedBy);
        void Update(Persistence.Entity.Table.TimeZone entity, Guid updatedBy);

        //public void Terminate(Persistence.Entity.Table.TimeZone timeZone, Guid updatedBy);
        //public void Reactivate(Persistence.Entity.Table.TimeZone timeZone, Guid updatedBy);
    }
}
