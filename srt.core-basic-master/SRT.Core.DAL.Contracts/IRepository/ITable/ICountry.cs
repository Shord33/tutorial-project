﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Table;

namespace SRT.Core.DAL.Contracts.IRepository.ITable
{
    public interface ICountry : IRepo<Country>
    {
        IEnumerable<Country> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<Country> GetByName(string name);
        IEnumerable<Country> ContainsName(string name);
        IEnumerable<Country> ContainsAliasName(string aliasName);
        IEnumerable<Country> ContainsDescription(string description);
        void Insert(Persistence.Entity.Table.Country entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Table.Country> entities, Guid updatedBy);
        void Update(Persistence.Entity.Table.Country entity, Guid updatedBy);

        //public void Terminate(Country country, Guid updatedBy);
        //public void Reactivate(Country country, Guid updatedBy);
    }
}
