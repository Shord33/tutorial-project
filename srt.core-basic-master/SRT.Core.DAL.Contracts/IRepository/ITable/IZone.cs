﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Persistence.Entity.Table;

namespace SRT.Core.DAL.Contracts.IRepository.ITable
{
    public interface IZone : IRepo<SRT.Core.Persistence.Entity.Table.Zone>
    {
        IEnumerable<Persistence.Entity.Table.Zone> GetByRefObjectState(Int64 refObjectState);
        IEnumerable<Persistence.Entity.Table.Zone> GetByName(string name);
        IEnumerable<Persistence.Entity.Table.Zone> ContainsName(string name);
        IEnumerable<Persistence.Entity.Table.Zone> ContainsAliasName(string aliasName);
        IEnumerable<Persistence.Entity.Table.Zone> ContainsDescription(string description);
        void Insert(Persistence.Entity.Table.Zone entity, Guid updatedBy);
        void InsertRange(IEnumerable<Persistence.Entity.Table.Zone> entities, Guid updatedBy);
        void Update(Persistence.Entity.Table.Zone entity, Guid updatedBy);

        //void Terminate(Persistence.Entity.Table.Zone timeZone, Guid updatedBy);
        //void Reactivate(Persistence.Entity.Table.Zone timeZone, Guid updatedBy);
    }
}
