﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Factory.Contract
{
    public interface IFactory
    {
        public IEnumerable<DTO.Factory> Get();
        public DTO.Factory GetByKey(Guid id);
        public bool Create(DTO.Factory factory, Guid updatedBy,bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.Factory> factory, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.Factory factory, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.Factory factory, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.Factory> GetActiveRefObjectState();
        public IEnumerable<DTO.Factory> GetTerminatedRefObjectState();
        public IEnumerable<DTO.Factory> ContainsName(string name);
        public IEnumerable<DTO.Factory> GetByName(string name);
        public IEnumerable<DTO.Factory> ContainsAliasName(string name);
        public IEnumerable<DTO.Factory> ContainsDescription(string description);
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
