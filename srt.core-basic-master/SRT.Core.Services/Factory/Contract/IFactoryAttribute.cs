﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Factory.Contract
{
    public interface IFactoryAttribute
    {
        public IEnumerable<DTO.FactoryAttribute> Get();
        public DTO.FactoryAttribute GetByKey(Guid id);
        public bool Create(DTO.FactoryAttribute factoryAttribute, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.FactoryAttribute> factoryAttribute, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.FactoryAttribute factoryAttribute, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy,bool CommitChanges = false);
        public bool Delete(DTO.FactoryAttribute factoryAttribute, Guid updatedBy,bool CommitChanges = false);
        public IEnumerable<DTO.FactoryAttribute> GetActiveRefObjectState();
        public IEnumerable<DTO.FactoryAttribute> GetTerminatedRefObjectState();
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
