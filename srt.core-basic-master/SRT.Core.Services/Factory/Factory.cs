﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using SRT.Core.DAL.Contracts.IUnitOfWork;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.Services.Factory.Contract;
using SRT.Core.Services.Enumeration;


namespace SRT.Core.Services.Factory
{
    public class Factory : IFactory
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgOrder = null;

        public Factory(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgOrder = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Factory.Factory, DTO.Factory>().ReverseMap();
            });
            this._mapper = _cfgOrder.CreateMapper();
        }

        public IEnumerable<DTO.Factory> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(this._uow.Factories.Get());
        }

        public DTO.Factory GetByKey(Guid id)
        {
            Persistence.Entity.Factory.Factory entity = this._uow.Factories.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Factory.Factory, DTO.Factory>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.Factory factory, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Factory obj = this.GetByName(factory.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            factory.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Factories.Insert(this._mapper.Map<DTO.Factory, Persistence.Entity.Factory.Factory>(factory), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.Factory> factories, Guid updatedBy, bool CommitChanges = false)
        {
            foreach (DTO.Factory t in factories)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;

        }

        public bool Update(DTO.Factory factory, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Factory obj = this.GetByKey(factory.IdFactory);
            if (obj == null)
            {
                return false;
            }

            this._uow.Factories.Update(this._mapper.Map<DTO.Factory, Persistence.Entity.Factory.Factory>(factory), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Factory obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }
            this._uow.Factories.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(DTO.Factory factory, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Factory.Factory entity = this._mapper.Map<DTO.Factory, Persistence.Entity.Factory.Factory>(factory);
            this._uow.Factories.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.Factory> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Factory.Factory> factories = this._uow.Factories.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(factories);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Factory> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Factory.Factory> factories = this._uow.Factories.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(factories);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Factory> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Factory.Factory> factories = this._uow.Factories.GetByName(name);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(factories);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DTO.Factory> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Factory.Factory> factories = this._uow.Factories.ContainsName(name);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(factories);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Factory> ContainsAliasName(string name)
        {
            IEnumerable<Persistence.Entity.Factory.Factory> factories = this._uow.Factories.ContainsAliasName(name);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(factories);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DTO.Factory> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Factory.Factory> factories = this._uow.Factories.ContainsDescription(description);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.Factory>, IEnumerable<DTO.Factory>>(factories);
            }
            else
            {
                return null;
            }
        }
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Factory.Factory factory = this._uow.Factories.GetByPKey(key);

            if (factory == null)
            {
                return false;
            }

            factory.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.Factories.Update(factory, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;

        }
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Factory.Factory factory = this._uow.Factories.GetByPKey(key);

            if (factory == null)
            {
                return false;
            }

            factory.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Factories.Update(factory, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }
    }
}
