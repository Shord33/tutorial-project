﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Factory.DTO
{
    public class FactoryAttribute
    {
        public Guid IdFactoryAttribute { get; set; }
        public Guid IdEntityTypeAttribute { get; set; }
        public string Value { get; set; }
        public Guid IdFactory { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }
    }
}
