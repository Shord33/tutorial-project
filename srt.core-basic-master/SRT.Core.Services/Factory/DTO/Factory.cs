﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Factory.DTO
{
    public class Factory
    {
        public Guid IdFactory { get; set; }
        //public long IdData { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }
    }
}
