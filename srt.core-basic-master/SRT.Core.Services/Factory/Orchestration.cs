﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;

using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Area;
using SRT.Core.Services.Factory.DTO;


namespace SRT.Core.Services.Factory
{
    public class Orchestration
    {
        IUnitOfWork _uow;
        Factory _factoryService;
        Area.Area _areaService;

        public Orchestration(IUnitOfWork uow)
        {
            this._uow = uow;
            this._factoryService = new Factory(this._uow);
            this._areaService = new Area.Area(this._uow);
        }
    }
}
