﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IUnitOfWork;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.Services.Factory.Contract;

namespace SRT.Core.Services.Factory
{
    public class FactoryAttribute : IFactoryAttribute
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgOrder = null;

        public FactoryAttribute(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgOrder = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Factory.FactoryAttribute, DTO.FactoryAttribute>().ReverseMap();
            });
            this._mapper = _cfgOrder.CreateMapper();
        }

        public IEnumerable<DTO.FactoryAttribute> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Factory.FactoryAttribute>, IEnumerable<DTO.FactoryAttribute>>(this._uow.FactoryAttributes.Get());
        }

        public DTO.FactoryAttribute GetByKey(Guid id)
        {
            Persistence.Entity.Factory.FactoryAttribute entity = this._uow.FactoryAttributes.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Factory.FactoryAttribute, DTO.FactoryAttribute>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.FactoryAttribute factoryAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.FactoryAttribute obj = this.GetByKey(factoryAttributes.IdFactory);
            if (obj != null)
            {
                return false;
            }

            factoryAttributes.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.FactoryAttributes.Insert(this._mapper.Map<DTO.FactoryAttribute, Persistence.Entity.Factory.FactoryAttribute>(factoryAttributes), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.FactoryAttribute> factories, Guid updatedBy, bool CommitChanges = false)
        {
            foreach (DTO.FactoryAttribute t in factories)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Update(DTO.FactoryAttribute factoryAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.FactoryAttribute obj = this.GetByKey(factoryAttributes.IdFactoryAttribute);
            if (obj == null)
            {
                return false;
            }

            this._uow.FactoryAttributes.Update(this._mapper.Map<DTO.FactoryAttribute, Persistence.Entity.Factory.FactoryAttribute>(factoryAttributes), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.FactoryAttribute obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }
            this._uow.FactoryAttributes.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(DTO.FactoryAttribute factoryAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Factory.FactoryAttribute entity = this._mapper.Map<DTO.FactoryAttribute, Persistence.Entity.Factory.FactoryAttribute>(factoryAttributes);
            this._uow.FactoryAttributes.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public IEnumerable<DTO.FactoryAttribute> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Factory.FactoryAttribute> factories = this._uow.FactoryAttributes.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.FactoryAttribute>, IEnumerable<DTO.FactoryAttribute>>(factories);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.FactoryAttribute> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Factory.FactoryAttribute> factories = this._uow.FactoryAttributes.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);

            if (factories != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Factory.FactoryAttribute>, IEnumerable<DTO.FactoryAttribute>>(factories);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Factory.FactoryAttribute factoryAttributes = this._uow.FactoryAttributes.GetByPKey(key);

            if (factoryAttributes == null)
            {
                return false;
            }

            factoryAttributes.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.FactoryAttributes.Update(factoryAttributes, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Factory.FactoryAttribute factoryAttributes = this._uow.FactoryAttributes.GetByPKey(key);

            if (factoryAttributes == null)
            {
                return false;
            }

            factoryAttributes.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.FactoryAttributes.Update(factoryAttributes, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }
    }
}
