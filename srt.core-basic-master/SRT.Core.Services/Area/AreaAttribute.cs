﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IUnitOfWork;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.Services.Area.Contract;

namespace SRT.Core.Services.Area
{
    public class AreaAttribute : IAreaAttribute
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgOrder = null;

        public AreaAttribute(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgOrder = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Area.AreaAttribute, DTO.AreaAttribute>().ReverseMap();
            });
            this._mapper = _cfgOrder.CreateMapper();
        }

        public IEnumerable<DTO.AreaAttribute> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Area.AreaAttribute>, IEnumerable<DTO.AreaAttribute>>(this._uow.AreaAttributes.Get());
        }

        public DTO.AreaAttribute GetByKey(Guid id)
        {
            Persistence.Entity.Area.AreaAttribute entity = this._uow.AreaAttributes.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Area.AreaAttribute, DTO.AreaAttribute>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.AreaAttribute areaAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.AreaAttribute obj = this.GetByKey(areaAttributes.IdArea);
            if (obj != null)
            {
                return false;
            }

            areaAttributes.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.AreaAttributes.Insert(this._mapper.Map<DTO.AreaAttribute, Persistence.Entity.Area.AreaAttribute>(areaAttributes), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.AreaAttribute> areaAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            foreach(DTO.AreaAttribute t in areaAttributes)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Update(DTO.AreaAttribute areaAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.AreaAttribute obj = this.GetByKey(areaAttributes.IdArea);
            if (obj == null)
            {
                return false;
            }

            this._uow.AreaAttributes.Update(this._mapper.Map<DTO.AreaAttribute, Persistence.Entity.Area.AreaAttribute>(areaAttributes), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.AreaAttribute obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }
            this._uow.AreaAttributes.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(DTO.AreaAttribute areaAttributes, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Area.AreaAttribute entity = this._mapper.Map<DTO.AreaAttribute, Persistence.Entity.Area.AreaAttribute>(areaAttributes);

            this._uow.AreaAttributes.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public IEnumerable<DTO.AreaAttribute> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Area.AreaAttribute> areaAttributes = this._uow.AreaAttributes.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);

            if (areaAttributes != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.AreaAttribute>, IEnumerable<DTO.AreaAttribute>>(areaAttributes);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.AreaAttribute> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Area.AreaAttribute> areaAttributes = this._uow.AreaAttributes.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);

            if (areaAttributes != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.AreaAttribute>, IEnumerable<DTO.AreaAttribute>>(areaAttributes);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Area.AreaAttribute areaAttributes = this._uow.AreaAttributes.GetByPKey(key);

            if (areaAttributes == null)
            {
                return false;
            }

            areaAttributes.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.AreaAttributes.Update(areaAttributes, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Area.AreaAttribute areaAttributes = this._uow.AreaAttributes.GetByPKey(key);

            if (areaAttributes == null)
            {
                return false;
            }

            areaAttributes.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.AreaAttributes.Update(areaAttributes, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }
    }
}
