﻿using SRT.Core.Services.Area.Request;
using SRT.Core.Services.Area.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.Contract
{
    public interface IOrchestration
    {
        public bool CheckAreaResource(Guid key);
        public CreateAreaResponse CreateArea(CreateAreaRequest request);
    }
}

