﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.Contract
{
    public interface IArea
    {
        public IEnumerable<DTO.Area> Get();
        public DTO.Area GetByKey(Guid id);
        public bool Create(DTO.Area area, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.Area> area, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.Area area, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.Area area, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.Area> GetActiveRefObjectState();
        public IEnumerable<DTO.Area> GetTerminatedRefObjectState();
        public IEnumerable<DTO.Area> ContainsName(string name);
        public IEnumerable<DTO.Area> GetByName(string name);
        public IEnumerable<DTO.Area> ContainsAliasName(string name);
        public IEnumerable<DTO.Area> ContainsDescription(string description);
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool SetSubArea(Guid area, Guid subArea, Guid updatedBy, bool CommitChanges = false);
    }
}
