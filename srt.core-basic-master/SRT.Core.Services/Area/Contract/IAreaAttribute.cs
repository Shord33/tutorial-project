﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.Contract
{
    public interface IAreaAttribute
    {
        public IEnumerable<DTO.AreaAttribute> Get();
        public DTO.AreaAttribute GetByKey(Guid id);
        public bool Create(DTO.AreaAttribute area, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.AreaAttribute> areaAttribute, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.AreaAttribute areaAttribute, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.AreaAttribute area, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.AreaAttribute> GetActiveRefObjectState();
        public IEnumerable<DTO.AreaAttribute> GetTerminatedRefObjectState();
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
