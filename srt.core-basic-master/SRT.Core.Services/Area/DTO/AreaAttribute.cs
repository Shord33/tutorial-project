﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.DTO
{
    public class AreaAttribute
    {
        public Guid IdAreaAttribute { get; set; }
        public Guid IdEntityTypeAttribute { get; set; }
        public string Value { get; set; }
        public Guid IdArea { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }
    }
}
