﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.DTO
{
    public class Area
    {
        public Guid IdArea { get; set; }
        public string? AreaCoordinate { get; set; }
        public Guid IdFactory { get; set; }
        public string Name { get; set; }
        public string? AliasName { get; set; }
        public string? Description { get; set; }
        public string? ImagePath { get; set; }
        public decimal? XAxis { get; set; }
        public decimal? YAxis { get; set; }
        public Guid? IdUserCreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid? IdUserUpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public long IdRefObjectState { get; set; }
        public Guid? IdTenant { get; set; }
        public Guid? IdAreaParent { get; set; }
        public bool? IsStorageArea { get; set; }
    }
}
