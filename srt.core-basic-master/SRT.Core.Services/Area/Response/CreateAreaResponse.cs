﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.Response
{
    public class CreateAreaResponse
    {
        public string Message { get; set; }
        public int Result { get; set; }
    }
}
