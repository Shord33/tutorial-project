﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Area.Request
{
    public class CreateAreaRequest
    {
        public DTO.Area areaObj { get; set; }
        public Guid updatedBy { get; set; }
        public string tenantName { get; set; }
    }
}
