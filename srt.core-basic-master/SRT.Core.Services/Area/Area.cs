﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.DAL.Contracts.IUnitOfWork;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.Services.Area.Contract;
using SRT.Core.Services.Enumeration;
using System.Linq;

namespace SRT.Core.Services.Area
{
    public class Area : IArea
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgOrder = null;

        public Area(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgOrder = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Area.Area, DTO.Area>().ReverseMap();
            });
            this._mapper = _cfgOrder.CreateMapper();
        }

        public IEnumerable<DTO.Area> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(this._uow.Areas.Get());
        }

        public DTO.Area GetByKey(Guid id)
        {
            Persistence.Entity.Area.Area entity = this._uow.Areas.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Area.Area, DTO.Area>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.Area area, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Area obj = this.GetByName(area.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            area.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Areas.Insert(this._mapper.Map<DTO.Area, Persistence.Entity.Area.Area>(area), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.Area> areas, Guid updatedBy, bool CommitChanges = false)
        {
            foreach (DTO.Area t in areas)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Update(DTO.Area area, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Area obj = this.GetByKey(area.IdArea);
            if (obj == null)
            {
                return false;
            }

            this._uow.Areas.Update(this._mapper.Map<DTO.Area, Persistence.Entity.Area.Area>(area), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Area obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }
            this._uow.Areas.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(DTO.Area area, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Area.Area entity = this._mapper.Map<DTO.Area, Persistence.Entity.Area.Area>(area);
            this._uow.Areas.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public IEnumerable<DTO.Area> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Area.Area> areas = this._uow.Areas.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);

            if (areas != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(areas);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Area> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Area.Area> areas = this._uow.Areas.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);

            if (areas != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(areas);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Area> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Area.Area> areas = this._uow.Areas.GetByName(name);

            if (areas != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(areas);
            }
            else
            {
                return null;
            }
        }
        
        public IEnumerable<DTO.Area> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Area.Area> areas = this._uow.Areas.ContainsName(name);

            if (areas != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(areas);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Area> ContainsAliasName(string name)
        {
            IEnumerable<Persistence.Entity.Area.Area> areas = this._uow.Areas.ContainsAliasName(name);

            if (areas != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(areas);
            }
            else
            {
                return null;
            }
        }
        
        public IEnumerable<DTO.Area> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Area.Area> areas = this._uow.Areas.ContainsDescription(description);

            if (areas != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Area.Area>, IEnumerable<DTO.Area>>(areas);
            }
            else
            {
                return null;
            }
        }
       
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Area.Area area = this._uow.Areas.GetByPKey(key);

            if (area == null)
            {
                return false;
            }

            area.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.Areas.Update(area, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }
       
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Area.Area area = this._uow.Areas.GetByPKey(key);

            if (area == null)
            {
                return false;
            }

            area.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Areas.Update(area, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool SetSubArea(Guid area, Guid subArea, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Area obj = this.GetByKey(area);
            if (obj == null)
            {
                return false;
            }

            DTO.Area sub = this.GetByKey(subArea);
            if (sub == null)
            {
                return false;
            }

            obj.IdAreaParent = null;
            sub.IdAreaParent = obj.IdArea;

            this.Update(obj, updatedBy);
            this.Update(sub, updatedBy);
            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }
    }
}
