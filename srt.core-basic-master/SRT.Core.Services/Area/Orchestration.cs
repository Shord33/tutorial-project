﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;


using SRT.Core.Services.Factory.DTO;
using SRT.Core.Services.Area.DTO;
using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Area.Request;
using SRT.Core.Services.Area.Response;
using SRT.Core.Services.Area.Contract;


namespace SRT.Core.Services.Area
{
    public class Orchestration : IOrchestration
    {
        private IMapper _mapper = null;
        private IUnitOfWork _uow;

        Area areaService;
        //Tenant tenantService;
        //Resource.Resource resourceService;

        public Orchestration(IUnitOfWork uow)
        {
            this._uow = uow;
            this.areaService = new Area(this._uow);
            //this.tenantService = new Tenant(this._uow);
            //this.resourceService = new Resource.Resource(this._uow);
        }

        public bool CheckAreaResource(Guid key)
        {
            //this.resourceService = new Resource.Resource(this._uow);
            //List<Resource.DTO.Resource> check = this.resourceService.Get().Where(x => x.IdArea == key).ToList();

            //if (check == null)
            //{
            //    return true;
            //}
            //else
            //{
                return false;
            //}
        }

        public CreateAreaResponse CreateArea(CreateAreaRequest request)
{
            this.areaService = new Area(this._uow);
            //this.tenantService = new Tenant(this._uow);

            CreateAreaResponse response = new CreateAreaResponse();
            DTO.Area area = new DTO.Area();
            Guid updatedBy = new Guid();

            //var tenant = tenantService.GetByName(request.tenantName).First();

            ///insert row in user table
            request.areaObj.IdArea = Guid.NewGuid();
            //request.areaObj.IdTenant = tenant.IdTenant;
            request.areaObj.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            request.areaObj.IdUserCreatedBy = request.updatedBy;

            area = request.areaObj;
            updatedBy = request.updatedBy;

            areaService.Create(area, updatedBy, true);

            return response;
        }
    }
}

