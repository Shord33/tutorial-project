﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services
{
    public class ErrorMessageConstant
    {
        public const string RecordExists = "Record already exists!";
        public const string RecordNotExists = "Record not exists!";
        public const string NameExists = "Name already exists!";
        public const string EmailExists = "Email already exists!";
        public const string OldPasswordNotMatch = "Old Password does not match!";
    }
}
