﻿using System;
using System.IO;
using System.Reflection;

namespace SRT.Core.Services
{
    internal class LogWriter
    {
        internal void WriteToFile(string msg)
        {
            var logDirectory = Path.Combine(AssemblyDirectory, "Log");

            if (!Directory.Exists(logDirectory)) Directory.CreateDirectory(logDirectory);

            var logFile = Path.Combine(logDirectory, DateTime.Now.ToString("yyyy-MM-dd") + "_AlarmEngine.txt");
            var logWriter = File.AppendText(logFile);

            logWriter.WriteLine(string.Format("{0} - {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), msg));
            logWriter.Close();
            logWriter.Dispose();
        }


        private string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}
