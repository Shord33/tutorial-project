﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Response
{
    public class GetLookUpTableValueByNameAndValueResponse
    {
        public IEnumerable<DTO.LookUpTableValue> lookUpTableValues { get; set; }
        public string Message { get; set; }
        public int Result { get; set; }
    }
}
