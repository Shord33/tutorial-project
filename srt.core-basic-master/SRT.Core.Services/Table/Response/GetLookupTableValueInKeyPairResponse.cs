﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Response
{
    public class GetLookupTableValueInKeyPairResponse
    {
        public IEnumerable<KeyValuePair<string, string>> keypairValues { get; set; }
        public string Message { get; set; }
        public int Result { get; set; }
    }
}
