﻿using System;
using System.Collections.Generic;
using System.Text;

using SRT.Core.Services.Enumeration;


namespace SRT.Core.Services.Table.Request
{
    public class GetLookupTableValueByNameRequest
    {
        public string columnName { get; set; }
    }
}
