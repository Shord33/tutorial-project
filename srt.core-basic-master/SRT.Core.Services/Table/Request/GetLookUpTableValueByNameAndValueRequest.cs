﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Request
{
    public class GetLookUpTableValueByNameAndValueRequest
    {
        public string columnName { get; set; }
        public int value { get; set; }
    }
}
