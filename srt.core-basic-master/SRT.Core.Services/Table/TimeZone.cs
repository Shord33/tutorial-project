﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Table.Contract;


namespace SRT.Core.Services.Table
{
    public class TimeZone : ITimeZone
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgAgent = null;

        public TimeZone(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgAgent = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Table.TimeZone, DTO.TimeZone>().ReverseMap();
            });
            this._mapper = _cfgAgent.CreateMapper();
        }

        public IEnumerable<DTO.TimeZone> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(this._uow.TimeZones.Get());
        }

        public DTO.TimeZone GetByKey(Guid id)
        {
            Persistence.Entity.Table.TimeZone entity = this._uow.TimeZones.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Table.TimeZone, DTO.TimeZone>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.TimeZone timeZone, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.TimeZone obj = this.GetByName(timeZone.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            timeZone.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.TimeZones.Insert(this._mapper.Map<DTO.TimeZone, Persistence.Entity.Table.TimeZone>(timeZone), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.TimeZone> timeZone, Guid updatedBy, bool CommitChanges = false)
        {
            //this._uow.TimeZones.InsertRange(this._mapper.Map<IEnumerable<DTO.TimeZone>, IEnumerable<Persistence.Entity.Table.TimeZone>>(timeZone));

            foreach (DTO.TimeZone t in timeZone)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Update(DTO.TimeZone timeZone, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.TimeZone obj = this.GetByKey(timeZone.IdTimeZone);
            if (obj == null)
            {
                return false;
            }

            this._uow.TimeZones.Update(this._mapper.Map<DTO.TimeZone, Persistence.Entity.Table.TimeZone>(timeZone), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.TimeZone obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }

            this._uow.TimeZones.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(DTO.TimeZone timeZone, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.TimeZone entity = this._mapper.Map<DTO.TimeZone, Persistence.Entity.Table.TimeZone>(timeZone);
            this._uow.TimeZones.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.TimeZone> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.TimeZone> timeZones = this._uow.TimeZones.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);
            if (timeZones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(timeZones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.TimeZone> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.TimeZone> timeZones = this._uow.TimeZones.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);
            if (timeZones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(timeZones);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DTO.TimeZone> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Table.TimeZone> timeZones = this._uow.TimeZones.ContainsName(name);
            if (timeZones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(timeZones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.TimeZone> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Table.TimeZone> timeZones = this._uow.TimeZones.GetByName(name);
            if (timeZones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(timeZones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.TimeZone> ContainsAliasName(string name)
        {
            IEnumerable<Persistence.Entity.Table.TimeZone> timeZones = this._uow.TimeZones.ContainsAliasName(name);
            if (timeZones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(timeZones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.TimeZone> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Table.TimeZone> timeZones = this._uow.TimeZones.ContainsDescription(description);
            if (timeZones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.TimeZone>, IEnumerable<DTO.TimeZone>>(timeZones);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.TimeZone timeZone = this._uow.TimeZones.GetByPKey(key);

            if (timeZone == null)
            {
                return false;
            }

            timeZone.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.TimeZones.Update(timeZone, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.TimeZone timeZone = this._uow.TimeZones.GetByPKey(key);

            if (timeZone == null)
            {
                return false;
            }

            timeZone.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.TimeZones.Update(timeZone, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }
    }
}
