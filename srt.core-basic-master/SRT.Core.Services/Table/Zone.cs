﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Table.Contract;


namespace SRT.Core.Services.Table
{
    public class Zone : IZone
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgAgent = null;

        public Zone(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgAgent = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Table.Zone, DTO.Zone>().ReverseMap();
            });
            this._mapper = _cfgAgent.CreateMapper();
        }

        public IEnumerable<DTO.Zone> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(this._uow.Zones.Get());
        }

        public DTO.Zone GetByKey(Guid id)
        {
            Persistence.Entity.Table.Zone entity = this._uow.Zones.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Table.Zone, DTO.Zone>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.Zone zone, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Zone obj = this.GetByName(zone.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            zone.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Zones.Insert(this._mapper.Map<DTO.Zone, Persistence.Entity.Table.Zone>(zone), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.Zone> zone, Guid updatedBy, bool CommitChanges = false)
        {
            //this._uow.Zones.InsertRange(this._mapper.Map<IEnumerable<DTO.Zone>, IEnumerable<Persistence.Entity.Table.Zone>>(zone));

            foreach (DTO.Zone t in zone)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Update(DTO.Zone zone, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Zone obj = this.GetByKey(zone.IdZone);
            if (obj == null)
            {
                return false;
            }

            this._uow.Zones.Update(this._mapper.Map<DTO.Zone, Persistence.Entity.Table.Zone>(zone), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Zone obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }

            this._uow.Zones.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy); 
            
            return true;
        }

        public bool Delete(DTO.Zone zone, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.Zone entity = this._mapper.Map<DTO.Zone, Persistence.Entity.Table.Zone>(zone);
            this._uow.Zones.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.Zone> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.Zone> zones = this._uow.Zones.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);
            if (zones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(zones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Zone> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.Zone> zones = this._uow.Zones.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);
            if (zones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(zones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Zone> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Table.Zone> zones = this._uow.Zones.ContainsName(name);
            if (zones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(zones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Zone> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Table.Zone> zones = this._uow.Zones.GetByName(name);
            if (zones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(zones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Zone> ContainsAliasName(string name)
        {
            IEnumerable<Persistence.Entity.Table.Zone> zones = this._uow.Zones.ContainsAliasName(name);
            if (zones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(zones);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Zone> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Table.Zone> zones = this._uow.Zones.ContainsDescription(description);
            if (zones != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Zone>, IEnumerable<DTO.Zone>>(zones);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.Zone zone = this._uow.Zones.GetByPKey(key);

            if (zone == null)
            {
                return false;
            }

            zone.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.Zones.Update(zone, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.Zone zone = this._uow.Zones.GetByPKey(key);

            if (zone == null)
            {
                return false;
            }

            zone.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Zones.Update(zone, updatedBy);
            
            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }
    }
}
