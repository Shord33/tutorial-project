﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.DTO
{
    public class LookUpTableValue
    {
        public long IdLookUpTableValue { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public int Value { get; set; }

        public long IdRefObjectState { get; set; }

        public long IdLookUpTable { get; set; }

        public long? IdLookUpTableValueParent { get; set; }

        public bool IsEnable { get; set; }

        public bool IsDefault { get; set; }

        public bool IsDisplay { get; set; }

        public Guid? IdTenant { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
