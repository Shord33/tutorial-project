﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.DTO
{
    public class LookUpTable
    {
        public long IdLookUpTable { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public Guid? IdModule { get; set; }

        public long IdRefObjectState { get; set; }

        public bool IsSystem { get; set; }

        public bool IsEditable { get; set; }

        //public Guid? IdTenant { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
