﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;



namespace SRT.Core.Services.Table.DTO
{
    public class Zone
    {
        public Guid IdZone { get; set; }

        public Guid IdCountry { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public long IdRefObjectState { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }

    }
}
