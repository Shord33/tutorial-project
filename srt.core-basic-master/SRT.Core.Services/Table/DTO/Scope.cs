﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.DTO
{
    public class Scope
    {
        public Guid IdScope { get; set; }

        public Guid IdModuleProvider { get; set; }

        public Guid IdModuleSubscriber { get; set; }

        public Guid IdEntityTypeProvider { get; set; }

        public Guid IdEntityTypeProviderInstance { get; set; }

        public Guid? IdEntityTypeSubscriber { get; set; }

        public Guid? IdEntityTypeSubscriberInstance { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Description { get; set; }

        public bool IsSystem { get; set; }

        public long IdRefObjectState { get; set; }

        public Guid? IdUserCreatedBy { get; set; }

        public Guid? IdUserUpdatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
