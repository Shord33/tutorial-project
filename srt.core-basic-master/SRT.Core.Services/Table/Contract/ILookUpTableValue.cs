﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Contract
{
    public interface ILookUpTableValue
    {
        public IEnumerable<DTO.LookUpTableValue> Get();
        public DTO.LookUpTableValue GetByKey(long id);
        public bool Create(DTO.LookUpTableValue lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.LookUpTableValue> lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.LookUpTableValue lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(long id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.LookUpTableValue lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.LookUpTableValue> GetActiveRefObjectState();
        public IEnumerable<DTO.LookUpTableValue> GetTerminatedRefObjectState();
        public IEnumerable<DTO.LookUpTableValue> ContainsName(string name);
        public IEnumerable<DTO.LookUpTableValue> GetByName(string name);
        public IEnumerable<DTO.LookUpTableValue> ContainsAliasName(string name);
        public IEnumerable<DTO.LookUpTableValue> ContainsDescription(string description);      
        public bool Terminate(long key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(long key, Guid updatedBy, bool CommitChanges = false);

        public IEnumerable<DTO.LookUpTableValue> GetMaintenancePlanType();
        public IEnumerable<DTO.LookUpTableValue> GetMaintenancePlanActivityType();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityExecutionType();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityCounterType();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityCounterEvent();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityAcceptanceMode();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivitReleaseMode();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivitBeginCompleteMode();
        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerType();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenanceSchedulerUnit();
        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerInterval();
        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerMode();
        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerNextMode();
        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenanceOrderState();       
        public IEnumerable<DTO.LookUpTableValue> GetByLookUpTable(long IdLookUpTable);
    }
}
