﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Contract
{
    public interface ILookUpTable
    {
        public IEnumerable<DTO.LookUpTable> Get();
        public DTO.LookUpTable GetByKey(long id);
        public bool Create(DTO.LookUpTable lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.LookUpTable> lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.LookUpTable lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(long id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.LookUpTable lookUpTable, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.LookUpTable> GetActiveRefObjectState();
        public IEnumerable<DTO.LookUpTable> GetTerminatedRefObjectState();
        public IEnumerable<DTO.LookUpTable> ContainsName(string name);
        public IEnumerable<DTO.LookUpTable> GetByName(string name);
        public IEnumerable<DTO.LookUpTable> ContainsAliasName(string name);
        public IEnumerable<DTO.LookUpTable> ContainsDescription(string description);
        public bool Terminate(long key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(long key, Guid updatedBy, bool CommitChanges = false);
    }
}
