﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Contract
{
    public interface IZone
    {
        public IEnumerable<DTO.Zone> Get();
        public DTO.Zone GetByKey(Guid id);
        public bool Create(DTO.Zone zone, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.Zone> zone, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.Zone zone, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.Zone zone, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.Zone> GetActiveRefObjectState();
        public IEnumerable<DTO.Zone> GetTerminatedRefObjectState();
        public IEnumerable<DTO.Zone> ContainsName(string name);
        public IEnumerable<DTO.Zone> GetByName(string name);
        public IEnumerable<DTO.Zone> ContainsAliasName(string name);
        public IEnumerable<DTO.Zone> ContainsDescription(string description);
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
