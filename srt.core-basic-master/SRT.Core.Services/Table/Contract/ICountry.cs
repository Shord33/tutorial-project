﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Contract
{
    public interface ICountry
    {
        public IEnumerable<DTO.Country> Get();
        public DTO.Country GetByKey(Guid id);
        public bool Create(DTO.Country country, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.Country> country, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.Country country, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.Country country, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.Country> GetActiveRefObjectState();
        public IEnumerable<DTO.Country> GetTerminatedRefObjectState();
        public IEnumerable<DTO.Country> ContainsName(string name);
        public IEnumerable<DTO.Country> GetByName(string name);
        public IEnumerable<DTO.Country> ContainsAliasName(string name);
        public IEnumerable<DTO.Country> ContainsDescription(string description);
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
