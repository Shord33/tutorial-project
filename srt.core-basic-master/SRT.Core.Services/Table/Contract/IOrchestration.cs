﻿using System;
using System.Collections.Generic;
using System.Text;


using SRT.Core.Services.Table;
using SRT.Core.Services.Table.Request;
using SRT.Core.Services.Table.Response;


namespace SRT.Core.Services.Table.Contract
{
    public interface IOrchestration
    {

        public GetLookupTableValueByNameResponse GetLookUpTableValueByName(GetLookupTableValueByNameRequest input);

        public GetLookupTableValueInKeyPairResponse GetLookUpTableValueInKeyPair(GetLookupTableValueInKeyPairRequest input);

        public GetLookUpTableValueByNameAndValueResponse GetLookUpTableValueByNameAndValue(GetLookUpTableValueByNameAndValueRequest input);
    }
}
