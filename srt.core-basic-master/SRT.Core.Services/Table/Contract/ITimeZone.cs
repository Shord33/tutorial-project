﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Table.Contract
{
    public interface ITimeZone
    {
        public IEnumerable<DTO.TimeZone> Get();
        public DTO.TimeZone GetByKey(Guid id);
        public bool Create(DTO.TimeZone timeZone, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.TimeZone> timeZone, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.TimeZone timeZone, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.TimeZone timeZone, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.TimeZone> GetActiveRefObjectState();
        public IEnumerable<DTO.TimeZone> GetTerminatedRefObjectState();
        public IEnumerable<DTO.TimeZone> ContainsName(string name);
        public IEnumerable<DTO.TimeZone> GetByName(string name);
        public IEnumerable<DTO.TimeZone> ContainsAliasName(string name);
        public IEnumerable<DTO.TimeZone> ContainsDescription(string description);
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
