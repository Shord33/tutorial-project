﻿using System;
using System.Collections.Generic;
using System.Linq;

using SRT.Core.DAL.Contracts.IUnitOfWork;
using SRT.Core.Services.Enumeration;
using SRT.Core.Services.Table.Contract;
using SRT.Core.Services.Table.Request;
using SRT.Core.Services.Table.Response;
using utility=SRT.Core.Utilies;

namespace SRT.Core.Services.Table
{
    public class Orchestration : IOrchestration
    {
        private IUnitOfWork _uow;


        public Orchestration(IUnitOfWork uow)
        {
            this._uow = uow;
        }

        private IEnumerable<DTO.LookUpTableValue> GetLookUpTableValueByName(Enumeration.Enum.LookUpTableName columnName)
        {
            LookUpTable lookUpTable = new LookUpTable(this._uow);
            LookUpTableValue lookUpTableValue = new LookUpTableValue(this._uow);

            IEnumerable<DTO.LookUpTable> lookUpTables = lookUpTable.GetByName(columnName.ToString());
            IEnumerable<DTO.LookUpTableValue> lookUpTableValues = lookUpTableValue.GetByLookUpTable(lookUpTables.SingleOrDefault().IdLookUpTable);

            return lookUpTableValues;
        }

        public GetLookupTableValueByNameResponse GetLookUpTableValueByName(GetLookupTableValueByNameRequest request)
        {
            GetLookupTableValueByNameResponse response = new GetLookupTableValueByNameResponse();

            System.Enum.TryParse(request.columnName, out Enumeration.Enum.LookUpTableName lookupTableColumnName);

            response.lookUpTableValues = GetLookUpTableValueByName(lookupTableColumnName);
            return response;
        }

        public GetLookupTableValueInKeyPairResponse GetLookUpTableValueInKeyPair(GetLookupTableValueInKeyPairRequest request)
        {
            utility.Lookup lookup = new utility.Lookup();
            GetLookupTableValueInKeyPairResponse response = new GetLookupTableValueInKeyPairResponse();

            System.Enum.TryParse(request.columnName, out Enumeration.Enum.LookUpTableName lookupTableColumnName);

            ///Only display the LookUpTableValue if IsDisplay = TRUE
            response.keypairValues = lookup.BindLookupToKeyPair(GetLookUpTableValueByName(lookupTableColumnName).Where(w => w.IsDisplay == true).ToDictionary(d => d.Value.ToString(), d => d.AliasName));
            return response;
        }

        public GetLookUpTableValueByNameAndValueResponse GetLookUpTableValueByNameAndValue(GetLookUpTableValueByNameAndValueRequest request)
        {
            GetLookUpTableValueByNameAndValueResponse response = new GetLookUpTableValueByNameAndValueResponse();
            System.Enum.TryParse(request.columnName, out Enumeration.Enum.LookUpTableName lookupTableColumnName);

            response.lookUpTableValues = GetLookUpTableValueByName(lookupTableColumnName).Where(x => x.Value == request.value);
            return response;
        }
    }
}
