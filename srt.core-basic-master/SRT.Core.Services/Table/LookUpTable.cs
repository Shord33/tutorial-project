﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Table.Contract;


namespace SRT.Core.Services.Table
{
    public class LookUpTable : ILookUpTable
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgAgent = null;

        public LookUpTable(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgAgent = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Table.LookUpTable, DTO.LookUpTable>().ReverseMap();
            });
            this._mapper = _cfgAgent.CreateMapper();
        }

        public IEnumerable<DTO.LookUpTable> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(this._uow.LookUpTables.Get());
        }

        public DTO.LookUpTable GetByKey(long id)
        {
            Persistence.Entity.Table.LookUpTable entity = this._uow.LookUpTables.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Table.LookUpTable, DTO.LookUpTable>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.LookUpTable lookUpTable, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.LookUpTable obj = this.GetByName(lookUpTable.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            lookUpTable.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.LookUpTables.Insert(this._mapper.Map<DTO.LookUpTable, Persistence.Entity.Table.LookUpTable>(lookUpTable), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.LookUpTable> lookUpTable, Guid updatedBy, bool CommitChanges = false)
        {
            //this._uow.LookUpTables.InsertRange(this._mapper.Map<IEnumerable<DTO.LookUpTable>, IEnumerable<Persistence.Entity.Table.LookUpTable>>(lookUpTable));
            
            foreach (DTO.LookUpTable t in lookUpTable)
            {
                Create(t, updatedBy);            
            }
            
            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Update(DTO.LookUpTable lookUpTable, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.LookUpTable obj = this.GetByKey(lookUpTable.IdLookUpTable);
            if (obj == null)
            {
                return false;
            }

            this._uow.LookUpTables.Update(this._mapper.Map<DTO.LookUpTable, Persistence.Entity.Table.LookUpTable>(lookUpTable), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(long id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.LookUpTable obj = this.GetByKey(id);
            if (obj == null || obj.IsSystem)
            {
                return false;
            }

            this._uow.LookUpTables.Delete(id);
            
            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(DTO.LookUpTable lookUpTable, Guid updatedBy, bool CommitChanges = false)
        {
            if(lookUpTable.IsSystem)
            {
                return false;
            }

            Persistence.Entity.Table.LookUpTable entity = this._mapper.Map<DTO.LookUpTable, Persistence.Entity.Table.LookUpTable>(lookUpTable);

            this._uow.LookUpTables.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.LookUpTable> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTable> lookUpTables = this._uow.LookUpTables.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);
            if (lookUpTables != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(lookUpTables);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTable> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTable> lookUpTables = this._uow.LookUpTables.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);
            if (lookUpTables != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(lookUpTables);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DTO.LookUpTable> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTable> lookUpTables = this._uow.LookUpTables.GetByName(name);
            if (lookUpTables != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(lookUpTables);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTable> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTable> lookUpTables = this._uow.LookUpTables.ContainsName(name);
            if (lookUpTables != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(lookUpTables);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTable> ContainsAliasName(string aliasName)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTable> lookUpTables = this._uow.LookUpTables.ContainsAliasName(aliasName);
            if (lookUpTables != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(lookUpTables);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTable> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTable> lookUpTables = this._uow.LookUpTables.ContainsDescription(description);
            if (lookUpTables != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTable>, IEnumerable<DTO.LookUpTable>>(lookUpTables);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(long key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.LookUpTable lookUpTable = this._uow.LookUpTables.GetByPKey(key);

            if(lookUpTable == null || lookUpTable.IsSystem)
            {
                return false;
            }

            lookUpTable.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.LookUpTables.Update(lookUpTable, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Reactivate(long key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.LookUpTable lookUpTable = this._uow.LookUpTables.GetByPKey(key);

            if (lookUpTable == null || lookUpTable.IsSystem)
            {
                return false;
            }

            lookUpTable.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.LookUpTables.Update(lookUpTable, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }



    }
}
