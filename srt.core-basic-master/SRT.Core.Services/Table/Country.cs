﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Table.Contract;


namespace SRT.Core.Services.Table
{
    public class Country : ICountry
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgAgent = null;

        public Country(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgAgent = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Table.Country, DTO.Country>().ReverseMap();
            });
            this._mapper = _cfgAgent.CreateMapper();
        }

        public IEnumerable<DTO.Country> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(this._uow.Countries.Get());
        }

        public DTO.Country GetByKey(Guid id)
        {
            Persistence.Entity.Table.Country entity = this._uow.Countries.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Table.Country, DTO.Country>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.Country country, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Country obj = this.GetByName(country.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            country.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Countries.Insert(this._mapper.Map<DTO.Country, Persistence.Entity.Table.Country>(country), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.Country> country, Guid updatedBy, bool CommitChanges = false)
        {
            //this._uow.Countries.InsertRange(this._mapper.Map<IEnumerable<DTO.Country>, IEnumerable<Persistence.Entity.Table.Country>>(country));

            foreach (DTO.Country t in country)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);
            
            return true;
        }

        public bool Update(DTO.Country country, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Country obj = this.GetByKey(country.IdCountry);
            if (obj == null)
            {
                return false;
            }

            this._uow.Countries.Update(this._mapper.Map<DTO.Country, Persistence.Entity.Table.Country>(country), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Country obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }

            this._uow.Countries.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(DTO.Country country, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.Country entity = this._mapper.Map<DTO.Country, Persistence.Entity.Table.Country>(country);
            this._uow.Countries.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.Country> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.Country> countries = this._uow.Countries.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);
            if (countries != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(countries);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Country> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.Country> countries = this._uow.Countries.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);
            if (countries != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(countries);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Country> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Table.Country> countries = this._uow.Countries.ContainsName(name);
            if (countries != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(countries);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Country> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Table.Country> countries = this._uow.Countries.GetByName(name);
            if (countries != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(countries);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Country> ContainsAliasName(string name)
        {
            IEnumerable<Persistence.Entity.Table.Country> countries = this._uow.Countries.ContainsAliasName(name);
            if (countries != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(countries);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Country> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Table.Country> countrys = this._uow.Countries.ContainsDescription(description);
            if (countrys != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.Country>, IEnumerable<DTO.Country>>(countrys);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.Country country = this._uow.Countries.GetByPKey(key);

            if (country == null)
            {
                return false;
            }

            country.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.Countries.Update(country, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.Country country = this._uow.Countries.GetByPKey(key);

            if (country == null)
            {
                return false;
            }

            country.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Countries.Update(country, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

    }
}
