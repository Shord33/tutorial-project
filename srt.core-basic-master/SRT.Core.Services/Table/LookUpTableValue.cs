﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;

using SRT.Core.Persistence.Entity;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.DAL.Contracts.IUnitOfWork;

using SRT.Core.Services.Table.Contract;


namespace SRT.Core.Services.Table
{
    public class LookUpTableValue : ILookUpTableValue
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgAgent = null;

        public LookUpTableValue(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgAgent = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Table.LookUpTableValue, DTO.LookUpTableValue>().ReverseMap();
            });
            this._mapper = _cfgAgent.CreateMapper();
        }

        public IEnumerable<DTO.LookUpTableValue> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(this._uow.LookUpTableValues.Get());
        }

        public DTO.LookUpTableValue GetByKey(long id)
        {
            Persistence.Entity.Table.LookUpTableValue entity = this._uow.LookUpTableValues.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Table.LookUpTableValue, DTO.LookUpTableValue>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.LookUpTableValue lookUpTableValue, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.LookUpTableValue obj = this.GetByName(lookUpTableValue.Name).SingleOrDefault();

            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            lookUpTableValue.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.LookUpTableValues.Insert(this._mapper.Map<DTO.LookUpTableValue, Persistence.Entity.Table.LookUpTableValue>(lookUpTableValue), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.LookUpTableValue> lookUpTableValue, Guid updatedBy, bool CommitChanges = false)
        {
            //this._uow.LookUpTableValues.InsertRange(this._mapper.Map<IEnumerable<DTO.LookUpTableValue>, IEnumerable<Persistence.Entity.Table.LookUpTableValue>>(lookUpTableValue));

            foreach (DTO.LookUpTableValue t in lookUpTableValue)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Update(DTO.LookUpTableValue lookUpTable, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.LookUpTableValue obj = this.GetByKey(lookUpTable.IdLookUpTable);
            if (obj == null)
            {
                return false;
            }

            this._uow.LookUpTableValues.Update(this._mapper.Map<DTO.LookUpTableValue, Persistence.Entity.Table.LookUpTableValue>(lookUpTable), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(long id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.LookUpTableValue obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }

            this._uow.LookUpTableValues.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Delete(DTO.LookUpTableValue lookUpTableValue, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.LookUpTableValue entity = this._mapper.Map<DTO.LookUpTableValue, Persistence.Entity.Table.LookUpTableValue>(lookUpTableValue);
            this._uow.LookUpTableValues.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.LookUpTableValue> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetByName(name);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.ContainsName(name);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> ContainsAliasName(string aliasName)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.ContainsAliasName(aliasName);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> ContainsDescription(string description)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.ContainsDescription(description);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public bool Terminate(long key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.LookUpTableValue lookUpTableValue = this._uow.LookUpTableValues.GetByPKey(key);

            if (lookUpTableValue == null)
            {
                return false;
            }

            lookUpTableValue.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.LookUpTableValues.Update(lookUpTableValue, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool Reactivate(long key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Table.LookUpTableValue lookUpTableValue = this._uow.LookUpTableValues.GetByPKey(key);

            if (lookUpTableValue == null)
            {
                return false;
            }

            lookUpTableValue.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.LookUpTableValues.Update(lookUpTableValue, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.LookUpTableValue> GetMaintenancePlanType()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetMaintenancePlanType();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetMaintenancePlanActivityType()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetMaintenancePlanActivityType();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityExecutionType()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenancePlanActivityExecutionType();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityCounterType()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenancePlanActivityCounterType();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityCounterEvent()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenancePlanActivityCounterEvent();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivityAcceptanceMode()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenancePlanActivityAcceptanceMode();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivitReleaseMode()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenancePlanActivitReleaseMode();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenancePlanActivitBeginCompleteMode()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenancePlanActivitBeginCompleteMode();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerType()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllSchedulerType();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenanceSchedulerUnit()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenanceSchedulerUnit();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerInterval()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllSchedulerInterval();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerMode()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllSchedulerMode();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllSchedulerNextMode()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllSchedulerNextMode();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetAllMaintenanceOrderState()
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetAllMaintenanceOrderState();
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.LookUpTableValue> GetByLookUpTable(long IdLookUpTable)
        {
            IEnumerable<Persistence.Entity.Table.LookUpTableValue> lookUpTableValues = this._uow.LookUpTableValues.GetByLookUpTable(IdLookUpTable);
            if (lookUpTableValues != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Table.LookUpTableValue>, IEnumerable<DTO.LookUpTableValue>>(lookUpTableValues);
            }
            else
            {
                return null;
            }
        }

    }
}
