﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Enumeration
{
    public class Enum
    {
        #region Entity

        public enum EntityType
        {
            IOTag = 1,
            AlarmInstance = 2,
            AlarmInstanceHistory = 3,
        }

        #endregion

        #region RefObjectState

        public enum RefObjectState
        {
            Terminated = 1,
            Active = 2,
            Draft = 3,
            Pending = 4,
            Effective = 5,
            Created = 6,
            Frozen = 7
        }

        #endregion

        #region LookUpTable

        public enum LookUpTableName
        {
            DatabaseOperation,
            TimeBasedUnitOfMeasure,
            SchedulerType,
            TimeBasedOccurs,
            ProductType,
            ResourceType,
            IOTagType,
            RecipeType,
            RecipeUnitProcedureType,
            RecipeUnitProcedureOperationType,
            RecipeUnitProcedureOperationPhaseType,
            ParameterType,
            ProcessingType,         
            DataType,
            DataTypeFormat,
            ScopeName,
            ModuleName,
            MaintenancePlanType,
            MaintenancePlanActivityType,
            MaintenancePlanActivityExecutionType,
            MaintenancePlanActivityCounterType,
            MaintenancePlanActivityCounterEvent,
            MaintenancePlanActivityAcceptanceMode,
            MaintenancePlanActivityReleaseMode,
            MaintenancePlanActivityBeginCompleteMode,
            MaintenanceSchedulerType,
            MaintenanceSchedulerUnit,
            MaintenanceSchedulerInterval,
            MaintenanceSchedulerMode,
            MaintenanceSchedulerNextMode,
            SchedulerWeekDay,
            SchedulerMonthDay,
            AlarmConditionType,
            AlarmConditionExpression,
            MessageTemplateType,
            NotificationType,
            RuleType
        }

        #endregion

        #region

        public enum DatabaseOperation
        {
            Update = 1,
            Create = 2,
            Terminate = 3
        }

        #endregion

        #region IOTag

        public enum IOTagType
        {
            Integrated = 1
        }

        #endregion

        #region Recipe
        public enum RecipeType
        {
            General = 1
        }


        public enum RecipeUnitProcedureOperationPhaseType
        {
            General = 1
        }

        public enum RecipeUnitProcedureOperationOperationType
        {
            General = 1
        }

        #endregion

        #region Resource
        public enum ProcessingType
        {
            Processing = 1
        }

        public enum ResourceType
        {
            General = 1
        }

        #endregion

        #region AlarmCondition

        public enum AlarmConditionType
        {
            Value = 1,
            IOTag = 2
        }

        public enum AlarmConditionExpression
        {
            GreaterThan = 1,
            LessThan = 2,
            Equal = 3
        }

        public enum DataType
        {
            Boolean = 1,
            Integer = 2,
            String = 3,
            Float = 4,
            Decimal = 5,
            DateTime = 6

        }

        public enum DataTypeFormat
        {
            DataTypeFormat1 = 1
        }

        #endregion

        #region AlarmState

        public enum AlarmState
        {
            OnAndNotAcknowledged = 1,
            OnAndAcknowledged = 2,
            OffAndNotAcknowledged = 3,
            OffAndAcknowledged = 4
        }

        #endregion

        #region Parameter
        public enum ParameterType
        {
            General = 1,
            Recipe = 2,
        }

        #endregion

        #region Scope

        public enum ScopeName
        {
            IOTag = 1,
            Alarm = 2,
        }

        public enum ModuleName
        {
            AlarmManagement = 1,
        }

        #endregion

        #region Maintenance

        public enum MaintenanceSchedulerType
        {
            TimeBased = 1,
            UsageBased = 2,
            TimeAndUsageBased = 3,
            Adhoc = 4
        }

        public enum MaintenancePlanActivityReleaseMode
        {
            AutoRelease = 1,
            ManualRelease = 2
        }

        public enum MaintenancePlanActivityAcceptanceMode
        {
            AutoAcceptance = 1,
            ManualAcceptance = 2
        }

        public enum MaintenancePlanActivityBeginCompleteMode
        {
            AutoBeginAndComplete = 1,
            ManualBeginAndComplete = 2
        }

        public enum MaintenancePlanActivityExecutionType
        {
            PerformExecution = 1,
            ReworkExecution = 2
        }

        public enum MaintenancePlanActivityCounterType
        {
            CountOne = 1,
            CountQuantityOne = 2,
            CountQuantityTwo = 3
        }

        public enum MaintenancePlanActivityCounterEvent
        {
            TrackIn = 1
        }

        public enum MaintenanceSchedulerMode
        {
            AnyDay = 1,
            PreviousWorkingDay = 2,
            NextWorkingDay = 3
        }

        public enum MaintenanceSchedulerNextMode
        {
            Fixed = 1,
            Dependent = 2
        }

        public enum MaintenanceSchedulerUnit
        {
            Minute = 1,
            Hour = 2,
            Day = 3,
            Week = 4,
            Month = 5
        }

        public enum MaintenanceSchedulerWeekDay
        {
            Monday = 1,
            Tuesday = 2,
            Wednesday = 3,
            Thursday = 4,
            Friday = 5,
            Saturday = 6,
            Sunday = 7
        }


        #endregion

        #region Notification

        public enum MessageType
        {
            SMS = 1,
            Email = 2
        }

        #endregion
    }
}
