﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Worker.Contract
{
    public interface IWorker
    {
        public IEnumerable<DTO.Worker> Get();
        public DTO.Worker GetByKey(Guid id);
        public bool Create(DTO.Worker worker, Guid updatedBy, bool CommitChanges = false);
        public bool CreateInBulk(IEnumerable<DTO.Worker> worker, Guid updatedBy, bool CommitChanges = false);
        public bool Update(DTO.Worker worker, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false);
        public bool Delete(DTO.Worker worker, Guid updatedBy, bool CommitChanges = false);
        public IEnumerable<DTO.Worker> GetActiveRefObjectState();
        public IEnumerable<DTO.Worker> GetTerminatedRefObjectState();
        public IEnumerable<DTO.Worker> ContainsName(string name);
        public IEnumerable<DTO.Worker> GetByName(string name);
        public IEnumerable<DTO.Worker> ContainsAliasName(string name);
        public IEnumerable<DTO.Worker> ContainsPosition(string position);
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false);
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false);
    }
}
