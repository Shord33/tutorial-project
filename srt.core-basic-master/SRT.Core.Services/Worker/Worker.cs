﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using SRT.Core.DAL.Contracts.IUnitOfWork;
using SRT.Core.DAL.UnitOfWork;
using SRT.Core.Services.Worker.Contract;
using SRT.Core.Services.Enumeration;


namespace SRT.Core.Services.Worker
{
    public class Worker : IWorker
    {
        private IUnitOfWork _uow;
        private IMapper _mapper = null;
        private MapperConfiguration _cfgOrder = null;

        public Worker(IUnitOfWork uow)
        {
            this._uow = uow;

            this._cfgOrder = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Persistence.Entity.Worker.Worker, DTO.Worker>().ReverseMap();
            });
            this._mapper = _cfgOrder.CreateMapper();
        }

        public IEnumerable<DTO.Worker> Get()
        {
            return this._mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(this._uow.Workers.Get());
        }

        public DTO.Worker GetByKey(Guid id)
        {
            Persistence.Entity.Worker.Worker entity = this._uow.Workers.GetByPKey(id);
            if (entity != null)
            {
                return this._mapper.Map<Persistence.Entity.Worker.Worker, DTO.Worker>(entity);
            }
            else
            {
                return null;
            }
        }

        public bool Create(DTO.Worker worker, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Worker obj = this.GetByName(worker.Name).SingleOrDefault();
            if (obj != null)
            {
                throw new Exception(ErrorMessageConstant.NameExists);
            }

            worker.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Workers.Insert(this._mapper.Map<DTO.Worker, Persistence.Entity.Worker.Worker>(worker), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public bool CreateInBulk(IEnumerable<DTO.Worker> workers, Guid updatedBy, bool CommitChanges = false)
        {
            foreach (DTO.Worker t in workers)
            {
                Create(t, updatedBy);
            }

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;

        }

        public bool Update(DTO.Worker worker, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Worker obj = this.GetByKey(worker.IdWorker);
            if (obj == null)
            {
                return false;
            }

            this._uow.Workers.Update(this._mapper.Map<DTO.Worker, Persistence.Entity.Worker.Worker>(worker), updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(Guid id, Guid updatedBy, bool CommitChanges = false)
        {
            DTO.Worker obj = this.GetByKey(id);
            if (obj == null)
            {
                return false;
            }
            this._uow.Workers.Delete(id);

            if (CommitChanges) this._uow.Complete(updatedBy);
            return true;
        }

        public bool Delete(DTO.Worker worker, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Worker.Worker entity = this._mapper.Map<DTO.Worker, Persistence.Entity.Worker.Worker>(worker);
            this._uow.Workers.Delete(entity);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }

        public IEnumerable<DTO.Worker> GetActiveRefObjectState()
        {
            IEnumerable<Persistence.Entity.Worker.Worker> workers = this._uow.Workers.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Active);

            if (workers != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(workers);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Worker> GetTerminatedRefObjectState()
        {
            IEnumerable<Persistence.Entity.Worker.Worker> workers = this._uow.Workers.GetByRefObjectState((Int64)Enumeration.Enum.RefObjectState.Terminated);

            if (workers != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(workers);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Worker> GetByName(string name)
        {
            IEnumerable<Persistence.Entity.Worker.Worker> workers = this._uow.Workers.GetByName(name);

            if (workers != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(workers);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DTO.Worker> ContainsName(string name)
        {
            IEnumerable<Persistence.Entity.Worker.Worker> workers = this._uow.Workers.ContainsName(name);

            if (workers != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(workers);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<DTO.Worker> ContainsAliasName(string name)
        {
            IEnumerable<Persistence.Entity.Worker.Worker> workers = this._uow.Workers.ContainsAliasName(name);

            if (workers != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(workers);
            }
            else
            {
                return null;
            }
        }
        public IEnumerable<DTO.Worker> ContainsPosition(string position)
        {
            IEnumerable<Persistence.Entity.Worker.Worker> workers = this._uow.Workers.ContainsPosition(position);

            if (workers != null)
            {
                return _mapper.Map<IEnumerable<Persistence.Entity.Worker.Worker>, IEnumerable<DTO.Worker>>(workers);
            }
            else
            {
                return null;
            }
        }
        public bool Terminate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Worker.Worker worker = this._uow.Workers.GetByPKey(key);

            if (worker == null)
            {
                return false;
            }

            worker.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Terminated;
            this._uow.Workers.Update(worker, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;

        }
        public bool Reactivate(Guid key, Guid updatedBy, bool CommitChanges = false)
        {
            Persistence.Entity.Worker.Worker worker = this._uow.Workers.GetByPKey(key);

            if (worker == null)
            {
                return false;
            }

            worker.IdRefObjectState = (Int64)Enumeration.Enum.RefObjectState.Active;
            this._uow.Workers.Update(worker, updatedBy);

            if (CommitChanges) this._uow.Complete(updatedBy);

            return true;
        }
    }
}
