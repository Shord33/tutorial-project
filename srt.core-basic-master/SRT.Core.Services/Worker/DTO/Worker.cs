﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services.Worker.DTO
{
    public class Worker
    {
        public Guid IdWorker { get; set; }

        public string Name { get; set; }

        public string AliasName { get; set; }

        public string Position { get; set; }

        public string WorkerCompanyEmail { get; set; }

        public long IdRefObjectState { get; set; }

        public string DbConnectionString { get; set; }

        public DateTime? JoinedDate { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
