﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRT.Core.Services
{
    public class RoleConstant
    {
        public const string SuperAdmin = "Super Admin";
        public const string Administrator = "Administrator";
    }
}
